import { IUser } from '../shared/types';
import { getAllUsers } from './users';

export function getAllUsers4Store(): IUser[] {
  return getAllUsers()
    .filter((user) => user.connected)
    .map((user) => {
      const { banned, connected, ...result } = user;
      return result;
    });
}
