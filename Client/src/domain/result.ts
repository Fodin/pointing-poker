import { getCurrentIssueId } from './game';

export type Vote = {
  userId: string;
  valueCard: string;
};

export type ResultOfCard = {
  issueId: string;
  votes: Vote[];
};

const result: ResultOfCard[] = [];

export function setResultCardValueUser(userId: string, cardValue: string): void {
  const currentIssueId = getCurrentIssueId();

  if (!result.some((resultOfCard: ResultOfCard) => resultOfCard.issueId === currentIssueId)) {
    result.push({ issueId: currentIssueId, votes: [] });
  }
  result.forEach((resultOfCard: ResultOfCard) => {
    if (resultOfCard.issueId === currentIssueId) {
      const indexVote = resultOfCard.votes.findIndex((vote) => vote.userId === userId);
      if (indexVote === -1) {
        resultOfCard.votes.push({ userId: userId, valueCard: cardValue });
      } else {
        resultOfCard.votes[indexVote].valueCard = cardValue;
      }
    }
  });
}

export function clearResultCurrentIssue(): void {
  const currentIssueId = getCurrentIssueId();
  const indexCurrentIssue = result.findIndex((itemResult) => {
    return itemResult.issueId === currentIssueId;
  });

  result.splice(indexCurrentIssue, 1);
}

export function getResultOfIssues(): ResultOfCard[] {
  return result.slice();
}
