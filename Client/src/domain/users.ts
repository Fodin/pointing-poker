export type User = {
  id: string;
  firstName: string;
  lastName?: string;
  jobPosition?: string;
  role: UserRole;
  src: string;
  connected: boolean;
  banned: boolean;
};

export enum UserRole {
  Dealer = '0',
  Player = '1',
  Observer = '2',
}

const users: User[] = [];

export function getAllUsers(): User[] {
  return users.slice();
}

export function getUserById(id: string): User | undefined {
  return users.find((user) => id === user.id);
}

export function isUserExists(id: string): boolean {
  return users.findIndex((user) => id === user.id) !== -1;
}

export function addNewUser(user: User): User | undefined {
  if (getUserById(user.id)) return;

  users.push(user);
  return user;
}

export function updateUser(user: Partial<User>): User | undefined {
  if (!user.id) return;

  const index = users.findIndex((el) => user.id === el.id);

  if (index === -1) return;

  users[index] = { ...users[index], ...user };
  return users[index];
}
