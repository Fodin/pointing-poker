export type ChatMessage = {
  id: string;
  userId: string;
  message: string;
};

const CHAT_LIMIT = 100;
const chat: ChatMessage[] = [];

export function addMessageToChat(message: ChatMessage): void {
  chat.push(message);
}

export function getAllChatMessages(): ChatMessage[] {
  return chat.slice();
}

export function getLimitedChatMessages(limit = CHAT_LIMIT): ChatMessage[] {
  return chat.slice(-limit);
}
