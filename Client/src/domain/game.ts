export type Game = {
  currentPage: number | undefined;
  isRoundRun: boolean;
  currentIssueId: string;
};

const game: Game = {
  currentPage: undefined,
  isRoundRun: false,
  currentIssueId: '',
};

export function setCurrentPage(currentPage: number): void {
  game.currentPage = currentPage;
}

export function setCurrentIssueId(currentIssueId: string): void {
  game.currentIssueId = currentIssueId;
}

export function setIsRoundRun(isRoundRun: boolean): void {
  game.isRoundRun = isRoundRun;
}

export function getCurrentIssueId(): string {
  return game.currentIssueId;
}

export function getCurrentPage(): number | undefined {
  return game.currentPage;
}
