export type Settings = {
  isDealerVotes: boolean;
  isSeeOthersVotes: boolean;
  isTimerNeeded: boolean;
  roundTime: number;
  scoreType: string;
  cardSet: string[];
};

const settings: Settings = {
  isDealerVotes: false,
  isSeeOthersVotes: false,
  isTimerNeeded: false,
  roundTime: 5,
  scoreType: 'SP',
  cardSet: ['Coffee', '?', '∞'],
};

export function addCardValue(value: string[]): void {
  settings.cardSet = [...settings.cardSet, ...value]
    .filter((itemValue) => itemValue.length > 0)
    .sort((a, b) => Number(a) - Number(b))
    .filter((itemValue, index, arr) => arr.indexOf(itemValue) === index && itemValue);
}

export function changeCardValue(value: { oldValue: string; newValue: string }): void {
  settings.cardSet = settings.cardSet
    .map((itemValue) => (itemValue === value.oldValue ? value.newValue : itemValue))
    .filter((itemValue) => String(itemValue).length > 0)
    .sort((a, b) => Number(a) - Number(b))
    .filter((itemValue, index, arr) => arr.indexOf(itemValue) === index && itemValue);
}

export function removeCardValue(value: string): void {
  settings.cardSet = settings.cardSet.filter((itemValue) => itemValue !== value);
}

export function getCardSet(): string[] {
  return settings.cardSet.slice();
}
