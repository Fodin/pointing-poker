export type Issue = {
  id: string;
  title: string;
  description: string;
  priority: string;
};

let issues: Issue[] = [];

export function addNewIssue(issue: Issue): void {
  issues.push(issue);
}

export function getAllIssues(): Issue[] {
  return issues.slice();
}

export function removeIssue(issueId: string): void {
  const foundIndex = issues.findIndex((el) => el.id === issueId);
  if (~foundIndex) {
    issues.splice(foundIndex, 1);
  }
}

export function changeTitle(issueId: string, title: string): void {
  const foundIndex = issues.findIndex((el) => el.id === issueId);
  if (~foundIndex) {
    issues[foundIndex].title = title;
  }
}

export function changeDescription(issueId: string, description: string): void {
  const foundIndex = issues.findIndex((el) => el.id === issueId);
  if (~foundIndex) {
    issues[foundIndex].description = description;
  }
}

export function replaceIssues(newIssues: Issue[]): void {
  issues = newIssues.slice();
}
