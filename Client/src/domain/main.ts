import { nanoid } from '@reduxjs/toolkit';

import store from '../redux/store';
import Types from '../redux/types';
import { addNewUser, getUserById, updateUser, User, UserRole } from './users';
import { addMessageToChat, getLimitedChatMessages, ChatMessage } from './chat';
import { sendFromDealerToUser } from '../shared/socketio';
import { me } from '../shared/localData';
import { IUser } from '../shared/types';
import { addCardValue, changeCardValue, getCardSet, removeCardValue } from './settings';
import {
  addNewIssue,
  changeTitle,
  changeDescription,
  Issue,
  removeIssue,
  replaceIssues,
} from './issues';

import { getCurrentPage, setCurrentIssueId, setCurrentPage, setIsRoundRun } from './game';
import { clearResultCurrentIssue, getResultOfIssues, setResultCardValueUser } from './result';

const {
  SET_IS_DEALER_VOTES,
  SET_IS_SEE_OTHERS_VOTES,
  IS_TIMER_NEEDED,
  SET_ROUND_TIME,
  SET_SCORE_TYPE,
  ADD_CARD_VALUE,
  CHANGE_CARD_VALUE,
  REMOVE_CARD_VALUE,
  SET_CARD_SET,
  SET_SETTINGS,
  USER_CONNECTED,
  USER_DISCONNECTED,
  CHAT_SEND_MESSAGE,
  ADD_NEW_ISSUE,
  REMOVE_ISSUE,
  CHANGE_ISSUE_TITLE,
  CHANGE_ISSUE_DESCRIPTION,
  STATE_UPDATE,
} = Types;

export function mainReducer(action: { type: string; value: any; senderId: string }): void {
  switch (action.type) {
    case USER_CONNECTED:
      //Добавить юзера в домен
      const user: IUser = action.value;
      const domainUser = getUserById(user.id);
      if (domainUser === undefined) {
        addNewUser({ ...user, connected: true, banned: false } as User);
      } else {
        if (!domainUser.banned) {
          updateUser({ ...user, connected: true, banned: false } as User);
        } else {
          stateUpdate([
            [
              'set error',
              {
                title: 'Error!',
                description: 'Sorry, you are kicked out of the game.',
              },
            ],
            ['set game.currentPage', 4],
          ]);
        }
      }

      if (me.id !== user.id) {
        //Послать весь стейт, если подключился не дилер
        console.log('sendFromDealerToUser(user.id, state);');
        sendFromDealerToUser(user.id, store.getState());
      }
      if (!getCurrentPage()) {
        setCurrentPage(1);
      }
      const oqlQuery: any = [
        ['set game.currentPage', getCurrentPage()],
        ['add users', user],
      ];
      if (me.id === user.id) {
        const message: ChatMessage = { id: nanoid(), userId: me.id, message: 'Hi!' };
        addMessageToChat(message);
        oqlQuery.push(['add chat', message]);
      }
      stateUpdate(oqlQuery);
      break;
    case USER_DISCONNECTED:
      const userId = action.value;
      //Обновить домен
      updateUser({ id: userId, connected: false });
      stateUpdate([`del users.id=${userId}`, 0]);
      break;
    case CHAT_SEND_MESSAGE:
      const newMessage = { id: nanoid(), userId: action.senderId, message: action.value };
      addMessageToChat(newMessage);
      // stateUpdate(['add chat', newMessage]);
      stateUpdate(['set chat', getLimitedChatMessages()]);
      break;

    case Types.SET_RESULT_CARD_VALUE: {
      const cardValue = action.value;
      const userId = action.senderId;
      setResultCardValueUser(userId, cardValue);
      stateUpdate([`set result`, getResultOfIssues()]);
      break;
    }
  }
  // Dealer-only actions
  console.log('Sender: %s', action.senderId);
  if (getUserById(action.senderId)?.role === UserRole.Dealer) {
    switch (action.type) {
      case ADD_NEW_ISSUE:
        const issue: Issue = action.value;
        addNewIssue(issue);
        stateUpdate([`add issues`, issue]);
        break;
      case REMOVE_ISSUE:
        const issueId = action.value;
        removeIssue(issueId);
        stateUpdate([`del issues.id=${issueId}`, 0]);
        break;
      case CHANGE_ISSUE_TITLE: {
        const { id: issueId, title } = action.value;
        changeTitle(issueId, title);
        stateUpdate([`set issues.id=${issueId}.title`, title]);
        // stateUpdate([`add issues.id=${issueId}`, { title }]);
        // stateUpdate(['set issues', getAllIssues()]);
        break;
      }
      case CHANGE_ISSUE_DESCRIPTION: {
        const { id: issueId, description } = action.value;
        changeDescription(issueId, description);
        stateUpdate([`set issues.id=${issueId}.description`, description]);
        break;
      }
      case Types.SET_NEW_ORDER_ISSUES: {
        const issues = action.value;
        replaceIssues(issues);
        stateUpdate([`set issues`, issues]);
        break;
      }
      case SET_IS_DEALER_VOTES:
        stateUpdate([`set settings.isDealerVotes`, action.value]);
        break;
      case SET_IS_SEE_OTHERS_VOTES:
        stateUpdate([`set settings.isSeeOthersVotes`, action.value]);
        break;
      case IS_TIMER_NEEDED:
        stateUpdate([`set settings.isTimerNeeded`, action.value]);
        break;
      case SET_ROUND_TIME:
        const time = action.value < 0 ? 0 : action.value;
        stateUpdate([`set settings.roundTime`, time]);
        break;
      case SET_SCORE_TYPE:
        stateUpdate([`set settings.scoreType`, action.value]);
        break;
      case SET_CARD_SET:
        stateUpdate([`set settings.cardSet`, action.value]);
        break;
      case ADD_CARD_VALUE:
        addCardValue(action.value);
        stateUpdate([`set cardArray`, getCardSet()]);
        break;
      case CHANGE_CARD_VALUE:
        changeCardValue(action.value);
        stateUpdate([`set cardArray`, getCardSet()]);
        break;
      case REMOVE_CARD_VALUE:
        removeCardValue(action.value);
        stateUpdate([`set cardArray`, getCardSet()]);
        break;
      case SET_SETTINGS:
        stateUpdate([`set settings`, action.value]);
        break;
      case Types.SET_CURRENT_PAGE: {
        const currentPage = action.value;
        setCurrentPage(currentPage);

        stateUpdate([`set game.currentPage`, currentPage]);
        break;
      }
      case Types.SET_CURRENT_ISSUE_ID: {
        const currentIssueId = action.value;
        setCurrentIssueId(currentIssueId);

        stateUpdate([`set game.currentIssueId`, currentIssueId]);
        break;
      }
      case Types.CLEAR_RESULT_CURRENT_ISSUE: {
        clearResultCurrentIssue();
        stateUpdate([`set result`, getResultOfIssues()]);
        break;
      }
      case Types.SET_IS_ROUND_RUN: {
        const isRoundRun = action.value;
        setIsRoundRun(isRoundRun);

        stateUpdate([`set game.isRoundRun`, isRoundRun]);
        break;
      }
    }
  }
}

function stateUpdate(oqlQuery: any) {
  store.dispatch({ type: STATE_UPDATE, value: oqlQuery });
}
