import { IUser } from './types';

export let me: IUser; // Local user data, initialized after client has connected to server

export function setMyData(data: IUser): void {
  me = data;
}
