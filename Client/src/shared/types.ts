import { PriorityIssue, UserRole } from './enums';

export interface IUser {
  id: string;
  role: UserRole;
  firstName: string;
  avatar?: string | ArrayBuffer | null;
  lastName?: string;
  position?: string;
}

export interface IGameCard {
  isEditable: boolean;
  value: string;
  renderLogo: JSX.Element;
}

export interface ICard {
  id: number;
  value: string;
  logo?: string;
}

export interface IIssue {
  id: string;
  title?: string;
  description?: string;
  priority?: PriorityIssue;
}

export interface IIssueResult {
  id: string;
  title: string;
  description: string;
  priority: string;
  result: string;
  averageResult: string;
}

export interface IVote {
  userId: string;
  valueCard: string;
}

export interface IResult {
  issueId: string;
  votes: IVote[];
}
