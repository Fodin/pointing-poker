import { io, Socket } from 'socket.io-client';
import { UserRole } from './enums';
import { mainReducer } from '../domain/main';
import store from '../redux/store';
import { me } from './localData';
import Types from '../redux/types';
import { Store } from '../redux/defaultState';

const { USER_CONNECTED, USER_DISCONNECTED, STATE_LOAD, STATE_UPDATE } = Types;

export let socket: Socket;

export function connect(gameId: string, userId: string, userRole: UserRole): void {
  socket = io({ query: { gameId, userId, userRole } });

  socket.on('connect', () => {
    store.dispatch({ type: USER_CONNECTED, value: me });
  });

  // This event sends only to dealer
  socket.on(
    'userDisconnected',
    (message: { socketId: string; userId: string; userRole: UserRole }) => {
      mainReducer({ type: USER_DISCONNECTED, value: message.userId, senderId: message.userId });
    },
  );

  socket.on('dealerDisconnected', () => {
    store.dispatch({
      type: STATE_UPDATE,
      value: [
        [
          'set error',
          {
            title: 'Error!',
            description: 'Sorry, the Dealer has been disconnected. Game is over.',
          },
        ],
        ['set game.currentPage', 4],
      ],
    });
    socket.disconnect();
  });

  socket.on('message', (message) => {
    store.dispatch(message);
  });

  socket.on('disconnect', () => {
    store.dispatch({
      type: STATE_UPDATE,
      value: [
        [
          'set error',
          {
            title: 'Error!',
            description:
              'Sorry, the connection has been broken. Try to reload this page and reconnect.',
          },
        ],
        ['set game.currentPage', 4],
      ],
    });
  });
}

export function sendAction(action: any): void {
  if (socket.connected) {
    socket.send(action);
  }
}

export function sendFromDealerToUser(userId: string, fullState: Store): void {
  if (socket.connected) {
    const message = { type: STATE_LOAD, value: fullState, senderId: userId };
    socket.emit('dealerToUser', message);
  }
}
