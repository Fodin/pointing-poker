import { IIssueResult, IResult } from './types';

const maxCardValueLength = 3;
const BOM = String.fromCharCode(0xfeff);

export const getFirstCharsOfWords = (...words: (string | undefined)[]): string => {
  let firstChars = '';
  words.forEach((word) => {
    if (!word) return;
    firstChars += word?.[0].toUpperCase();
  });
  return firstChars;
};

export const getMinutes = (timerValue: number): string => {
  const minutes = ('' + Math.floor(timerValue / 60)).slice(-2);
  return minutes;
};

export const getSeconds = (timerValue: number): string => {
  const seconds = ('0' + Math.floor(timerValue % 60)).slice(-2);
  return seconds;
};

export const getOffsetToLeftForOneItem = (
  widthContainer: number,
  numberItem: number,
  widthItem: number,
): number => {
  const offset = (widthContainer - widthItem * numberItem) / (numberItem - 1) + widthItem;
  return offset;
};

export const checkValueLength = (value: string): string => {
  const newValue = value
    .split(' ')
    .map((itemValue) => itemValue.replace(/[^0-9]/g, ''))
    .map((itemValue) =>
      itemValue === '' ? itemValue : Number(itemValue.slice(0, maxCardValueLength)),
    );
  return newValue.join(' ');
};

export const checkValueSpace = (value: string): string => {
  return checkValueLength(value).split(' ').join('').slice(0, maxCardValueLength);
};

export const resizeImage = (
  file: { originFileObj: Blob },
  maxWidth = 64,
  maxHeight = 64,
): Promise<string> => {
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.readAsDataURL(file.originFileObj);
    const img = new Image();
    reader.onload = () => {
      img.src = reader.result + '';
      img.onload = () => {
        const canvas = document.createElement('canvas');
        const MAX_WIDTH = maxWidth;
        const MAX_HEIGHT = maxHeight;
        let width = img.width;
        let height = img.height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;
        const ctx = canvas.getContext('2d');
        ctx?.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL());
      };
    };
  });
};

export const getColor = (id: string): string => {
  const number = id.split('').reduce((acc, ch) => acc + ch.charCodeAt(0), 0) % 360;

  return `hsl(${number}, 75%, 60%)`;
};

export const getPercentForEachCardOfIssue = (
  statistic: IResult[],
  issueId: string,
): [string, string][] => {
  const currentIssueVoteArray = statistic.find((issue) => issue.issueId === issueId)?.votes;
  const partOfHundred = currentIssueVoteArray ? Math.round(100 / currentIssueVoteArray.length) : 0;

  interface IObjectKeys {
    [key: string]: string;
  }
  const resultStatistic: IObjectKeys = {};
  currentIssueVoteArray?.forEach((item) => {
    if (resultStatistic[item.valueCard] !== undefined) {
      resultStatistic[item.valueCard] = `${+resultStatistic[item.valueCard] + +partOfHundred}`;
    } else {
      resultStatistic[item.valueCard] = partOfHundred + '';
    }
  });

  return Object.entries(resultStatistic);
};

export const saveFileAsTxt = (data: IIssueResult[]): void => {
  const element = document.createElement('a');
  const textFile = new Blob(
    [
      JSON.stringify(
        data,
        (key, value) => {
          if (key === 'id') return undefined;
          return value;
        },
        2,
      ).replace(/[\[{("")}\]]/g, ''),
    ],
    {
      type: 'text/plain',
    },
  );
  element.href = URL.createObjectURL(textFile);
  element.download = 'pointing_poker.txt';
  document.body.appendChild(element);
  element.click();
};

export const saveFileAsJson = (data: IIssueResult[]): void => {
  const element = document.createElement('a');
  const textFile = new Blob(
    [JSON.stringify(data, (key, value) => (key === 'id' ? undefined : value), 2)],
    {
      type: 'contentType',
    },
  );
  element.href = URL.createObjectURL(textFile);
  element.download = 'pointing_poker.json';
  document.body.appendChild(element);
  element.click();
};

export const saveFileAsCSV = (data: IIssueResult[]): void => {
  const str = convertToCSV(data);
  const element = document.createElement('a');
  const csvData = new Blob([BOM + str], { type: 'text/csv' });
  element.href = URL.createObjectURL(csvData);
  element.download = 'pointing_poker.csv';
  document.body.appendChild(element);
  element.click();
};

export const getAverageResult = (resultIssue: [string, string][], scoreType: string): string => {
  let totalAverageResult = 0;
  let percent = 0;
  resultIssue.forEach((item) => {
    if (!isNaN(Number(item[0]))) {
      totalAverageResult += (+item[0] * +item[1]) / 100;
      percent += +item[1];
    }
  });
  const averageResult = percent
    ? `${Math.round((totalAverageResult * 100) / percent)} ${scoreType}`
    : '';
  return averageResult;
};

export const convertToCSV = (objArray: IIssueResult[]): string => {
  const headerArr = ['title', 'priority', 'description', 'averageResult', 'result'];

  objArray.forEach((obj) => {
    for (const prop in obj) {
      obj[prop as keyof IIssueResult] = obj[prop as keyof IIssueResult].replaceAll('"', "'");
    }
  });
  const replacer = (key: string, value: string) => (value === null ? '' : value);
  const csv = [
    headerArr.join(';'),
    ...objArray.map((row) =>
      headerArr
        .map((fieldName) => JSON.stringify(row[fieldName as keyof IIssueResult], replacer))
        .join(';'),
    ),
  ].join('\r\n');
  return csv;
};

export const getIssuesFromPC = (
  file: { originFileObj: Blob },
  setIssueFromFile: React.Dispatch<React.SetStateAction<string | ArrayBuffer | null>>,
): void => {
  const reader = new FileReader();
  reader.readAsText(file.originFileObj);
  reader.onload = () => {
    setIssueFromFile(reader.result);
    console.log(reader.result);
  };
};
