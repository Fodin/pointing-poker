export enum PriorityIssue {
  LOW = 'Low',
  MIDDLE = 'Normal',
  HIGHT = 'High',
}

export enum ScoreTypeName {
  SP_NAME = 'Story Point',
  TSHIRT_NAME = 'T-shirt',
  HOURS_NAME = 'Hours',
  DAYS_NAME = 'Days',
  OTHER_NAME = 'Other',
}

export enum ScoreType {
  SP = 'SP',
  TSHIRT = 'TS',
  HOURS = 'HR',
  DAYS = 'DS',
  CUSTOM = 'Custom',
}

export enum CardSet {
  FIBONACCI = 'Fibonacci (0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89)',
  M_FIBONACCI = 'Modified Fibonacci (0, ½, 1, 2, 3, 5, 8, 13, 20, 40, 100)',
  TSHIRTS = 'T-shirts (xxs, xs, s, m, l, xl, xxl)',
  POWERS_2 = 'Powers of 2 (0, 1, 2, 4, 8, 16, 32, 64)',
  OTHER = 'Other',
}

export enum CardSetArray {
  FIBONACCI_ARRAY = 'Coffee,?,∞,0,1,2,3,5,8,13,21,34,55,89',
  M_FIBONACCI_ARRAY = 'Coffee,?,∞,0,½,1,2,3,5,8,13,20,40,100',
  TSHIRTS_ARRAY = 'Coffee,?,∞,xxs,xs,s,m,l,xl,xxl',
  POWERS_2_ARRAY = 'Coffee,?,∞,0,1,2,4,8,16,32,64',
  OTHER_ARRAY = 'Coffee,?,∞',
}

export enum CardValue {
  COFFEE = 'Coffee',
  QUESTION = '?',
  INFINITY = '∞',
}

export enum Other {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  UNDEFINED = 'undefined',
}

export enum UserRole {
  Dealer = '0',
  Player = '1',
  Observer = '2',
}
