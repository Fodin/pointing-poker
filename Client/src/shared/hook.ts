import { RefObject, useEffect, useState } from 'react';

export const useContainerWidth = (myRef: RefObject<HTMLDivElement>): number => {
  const [fieldWidth, setFieldWidth] = useState(0);
  useEffect(() => {
    const handleResize = () => {
      if (myRef.current) setFieldWidth(myRef.current.offsetWidth);
    };
    if (myRef.current) {
      setFieldWidth(myRef.current.offsetWidth);
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [myRef]);

  return fieldWidth;
};
