const STORAGE_PREFIX = 'GYF-PP';

export default class Storage {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
  static save(key: string, value: any): void {
    localStorage.setItem(`${STORAGE_PREFIX}-${key}`, JSON.stringify(value));
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static load(key: string): any {
    const rawValue = localStorage.getItem(`${STORAGE_PREFIX}-${key}`);

    if (rawValue === null) return null;

    return JSON.parse(rawValue);
  }

  static remove(key: string): void {
    localStorage.removeItem(`${STORAGE_PREFIX}-${key}`);
  }

  static clear(): void {
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      if (key?.startsWith(STORAGE_PREFIX)) {
        localStorage.removeItem(key);
      }
    }
  }
}
