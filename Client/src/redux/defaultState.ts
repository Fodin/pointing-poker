import { IResult, IUser, IIssue } from '../shared/types';

export type Store = {
  cardArray: string[];
  settings: {
    isDealerVotes: boolean;
    isSeeOthersVotes: boolean;
    isTimerNeeded: boolean;
    roundTime: number;
    scoreType: string;
    cardSet: string[];
  };
  users: IUser[];
  chat: {
    id: string;
    userId: string;
    message: string;
  }[];
  issues: IIssue[];
  result: IResult[];
  game: {
    currentPage: number | undefined;
    isRoundRun: boolean;
    currentIssueId: string;
  };
  error: {
    title: string;
    description: string;
  };
};

const store: Store = {
  cardArray: ['Coffee', '?', '∞'],
  settings: {
    isDealerVotes: false,
    isSeeOthersVotes: false,
    isTimerNeeded: false,
    roundTime: 5,
    scoreType: 'SP',
    cardSet: ['Coffee', '?', '∞', '0', '1', '2', '3', '5', '8', '13', '21', '34', '55', '89'],
  },
  issues: [],
  users: [],
  chat: [],
  result: [],
  game: {
    currentPage: undefined,
    isRoundRun: false,
    currentIssueId: '',
  },
  error: {
    title: 'Error!',
    description: 'Something goes wrong!',
  },
};

export default store;
