import { me } from '../shared/localData';
import { UserRole } from '../shared/enums';
import { mainReducer } from '../domain/main';
import Types from './types';
import { sendAction } from '../shared/socketio';

const incomingActionTypes: string[] = ['@@INIT', Types.STATE_LOAD, Types.STATE_UPDATE];

export function dispatcher(store: any) {
  return function (next: any) {
    return function (action: any) {
      console.group('Middleware is dispatching', action.type);
      console.log('store: %o', store.getState());
      console.log('action: %o', action);
      console.groupEnd();

      // Обработка ответов от дилера
      if (incomingActionTypes.includes(action.type)) {
        if (me.role === UserRole.Dealer) {
          sendAction(action);
        }
        return next(action);
      }

      switch (me.role) {
        case UserRole.Dealer: // Событие, созданное дилером, улетает сразу в обработку
          mainReducer({ senderId: me.id, ...action });
          break;
        case UserRole.Player: // Событие улетает по сети дилеру на обработку
          sendAction(action);
          break;
        case UserRole.Observer:
          console.log('Observer action %o', action);
          if (Types.USER_CONNECTED === action.type) {
            sendAction(action);
          }
          break;
      }
    };
  };
}
