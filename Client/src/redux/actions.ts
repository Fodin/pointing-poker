import { Settings } from '../domain/settings';
import { IIssue } from '../shared/types';
import { IUser } from '../shared/types';
import Type from './types';

const {
  CHAT_SEND_MESSAGE,
  ADD_CARD_VALUE,
  CHANGE_CARD_VALUE,
  REMOVE_CARD_VALUE,

  SET_IS_DEALER_VOTES,
  SET_IS_SEE_OTHERS_VOTES,
  IS_TIMER_NEEDED,
  SET_ROUND_TIME,
  SET_SCORE_TYPE,
  SET_CARD_SET,
  SET_SETTINGS,

  ADD_NEW_ISSUE,
  CHANGE_ISSUE_TITLE,
  CHANGE_ISSUE_DESCRIPTION,
  REMOVE_ISSUE,
  SET_NEW_ORDER_ISSUES,

  ADD_USER_TO_ARRAY,
  REMOVE_USER,

  SET_PRIVATE_USER_ID,
  SET_USERS,
  SET_IS_ROUND_RUN,
  SET_CURRENT_ISSUE_ID,
  SET_RESULT_CARD_VALUE,
  CLEAR_RESULT_CURRENT_ISSUE,

  SET_CURRENT_PAGE,
} = Type;

export function chatSendMessage(value: string): { type: string; value: string } {
  return { type: CHAT_SEND_MESSAGE, value };
}

export function addCardValue(value: string[] | number[]): {
  type: string;
  value: string[] | number[];
} {
  return { type: ADD_CARD_VALUE, value };
}

export function changeCardValue(value: {
  oldValue: string | number | undefined;
  newValue: string | number | undefined;
}): {
  type: string;
  value: { oldValue: string | number | undefined; newValue: string | number | undefined };
} {
  return { type: CHANGE_CARD_VALUE, value };
}

export function removeCardValue(value: string | number | undefined): {
  type: string;
  value: string | number | undefined;
} {
  return { type: REMOVE_CARD_VALUE, value };
}

export function setIsDealerVotes(value: boolean): {
  type: string;
  value: boolean;
} {
  return { type: SET_IS_DEALER_VOTES, value };
}

export function setIsSeeOthersVotes(value: boolean): {
  type: string;
  value: boolean;
} {
  return { type: SET_IS_SEE_OTHERS_VOTES, value };
}

export function setIsTimerNeeded(value: boolean): {
  type: string;
  value: boolean;
} {
  return { type: IS_TIMER_NEEDED, value };
}

export function setRoundTime(value: number): {
  type: string;
  value: number;
} {
  return { type: SET_ROUND_TIME, value };
}

export function setScoreType(value: string): {
  type: string;
  value: string;
} {
  return { type: SET_SCORE_TYPE, value };
}

export function setCardSet(value: number[] | string[]): {
  type: string;
  value: number[] | string[];
} {
  return { type: SET_CARD_SET, value };
}

export function setSettings(value: Settings): {
  type: string;
  value: Settings;
} {
  return { type: SET_SETTINGS, value };
}

export function addNewIssue(value: IIssue): {
  type: string;
  value: IIssue;
} {
  return { type: ADD_NEW_ISSUE, value };
}

export function changeIssueTitle(value: IIssue): {
  type: string;
  value: IIssue;
} {
  return { type: CHANGE_ISSUE_TITLE, value };
}

export function changeIssueDescription(value: IIssue): {
  type: string;
  value: IIssue;
} {
  return { type: CHANGE_ISSUE_DESCRIPTION, value };
}

export function removeIssue(value: string | number): {
  type: string;
  value: string | number;
} {
  return { type: REMOVE_ISSUE, value };
}

export function addUserToArray(value: IUser): {
  type: string;
  value: IUser;
} {
  return { type: ADD_USER_TO_ARRAY, value };
}

export function removeUser(value: string | number): {
  type: string;
  value: string | number;
} {
  return { type: REMOVE_USER, value };
}

export function setPrivateUserId(value: string): {
  type: string;
  value: string;
} {
  return { type: SET_PRIVATE_USER_ID, value };
}

export function setUsers(value: IUser[]): {
  type: string;
  value: IUser[];
} {
  return { type: SET_USERS, value };
}

export function setIsRoundRun(value: boolean): {
  type: string;
  value: boolean;
} {
  return { type: SET_IS_ROUND_RUN, value };
}

export function setCurrentIssueId(value: string): {
  type: string;
  value: string;
} {
  return { type: SET_CURRENT_ISSUE_ID, value };
}

export function setResultCardValue(value: string | number): {
  type: string;
  value: string | number;
} {
  return { type: SET_RESULT_CARD_VALUE, value };
}

export function clearResultCurrentIssue(): {
  type: string;
} {
  return { type: CLEAR_RESULT_CURRENT_ISSUE };
}

export function setNewOrderIssues(value: IIssue[]): {
  type: string;
  value: IIssue[];
} {
  return { type: SET_NEW_ORDER_ISSUES, value };
}

export function setCurrentPage(value: number): {
  type: string;
  value: number;
} {
  return { type: SET_CURRENT_PAGE, value };
}
