import { Store } from './defaultState';

export const selectCardArray = ({ cardArray }: Store): Store['cardArray'] => cardArray;

export const selectSettings = ({ settings }: Store): Store['settings'] => settings;

export const selectChat = ({ chat }: Store): Store['chat'] => chat;

export const selectGame = ({ game }: Store): Store['game'] => game;

export const selectIssues = ({ issues }: Store): Store['issues'] => issues;

export const selectUsers = ({ users }: Store): Store['users'] => users;

export const selectIsRoundRun = ({ game: { isRoundRun } }: Store): Store['game']['isRoundRun'] =>
  isRoundRun;

export const selectCurrentIssueId = ({
  game: { currentIssueId },
}: Store): Store['game']['currentIssueId'] => currentIssueId;

export const selectStatistic = ({ result }: Store): Store['result'] => result;

export const selectError = ({ error }: Store): Store['error'] => error;
