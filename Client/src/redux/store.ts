import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { Other } from '../shared/enums';

import rootReducer from './reducer';
import { dispatcher } from './middleware';

const { PRODUCTION, UNDEFINED } = Other;

const composeSwitch =
  (process.env.NODE_ENV !== PRODUCTION &&
    typeof window !== UNDEFINED &&
    composeWithDevTools(applyMiddleware(thunk, dispatcher))) ||
  compose(applyMiddleware(thunk, dispatcher));

export default createStore(rootReducer, composeSwitch);
