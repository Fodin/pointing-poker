import defaultState from './defaultState';
import Type from './types';
import { UserRole } from '../shared/enums';
import { IUser } from '../shared/types';
import { oql } from 'oql-lib';
import { me } from '../shared/localData';
const { Dealer } = UserRole;

const {
  STATE_UPDATE,
  STATE_LOAD,
  FATAL_ERROR,

  ADD_NEW_ISSUE,
  CHANGE_ISSUE_TITLE,
  CHANGE_ISSUE_DESCRIPTION,
  REMOVE_ISSUE,

  ADD_USER_TO_ARRAY,
  REMOVE_USER,

  SET_PRIVATE_USER_ID,
  SET_USERS,
} = Type;

export default (state = defaultState, { type, value }: { type: string; value: any }): any => {
  // console.log('Came in reducer: %s, %o ', type, value);
  switch (type) {
    case STATE_UPDATE:
      // console.group('Как обновится state в редьюсере:');
      // console.log('oql: %o, state: %o', value, state);
      // console.dir(oql(value, state));
      // console.groupEnd();
      return oql(value, state);
    case STATE_LOAD:
      return { ...value };
    // case FATAL_ERROR:
    //   /// Что делать при фатальной ошибке
    //   return;
    ///////////////////////////////////////////////////////
    // case ADD_NEW_ISSUE:
    //   return { ...state, issues: [...state.issues, value] };
    /*case CHANGE_ISSUE_TITLE:
      const indexTitle = state.issues.findIndex(({ issueId }: IIssue) => issueId === value.issueId);
      const objTitle: IIssue = state.issues[indexTitle];
      objTitle.title = value.title;
      return { ...state, issues: state.issues };
    case CHANGE_ISSUE_DESCRIPTION:
      const indexDescription = state.issues.findIndex(
        ({ issueId }: IIssue) => issueId === value.issueId,
      );
      const objDescription: IIssue = state.issues[indexDescription];
      objDescription.description = value.description;
      return { ...state, issues: state.issues };
    case REMOVE_ISSUE:
      return { ...state, issues: state.issues.filter(({ issueId }) => issueId !== value) };
      return { ...state, issues: state.issues.filter(({ id }) => issueId !== value) };
    case ADD_USER_TO_ARRAY:
      return { ...state, users: [...state.users, value] };
    case REMOVE_USER:
      return { ...state, users: state.users.filter(({ id }) => userId !== value) };*/
    /*case SET_USERS:
      const dealerIndex = value.findIndex((user: IUser) => user.role === Dealer);

      const privateUserIndex = value.findIndex((user: IUser) => user.id === me.id);

      const newValue = [...value];
      const newUsersList = [
        ...newValue.splice(dealerIndex, 1),
        ...newValue.splice(privateUserIndex - 1, 1),
        ...newValue,
      ];
      return { ...state, users: newUsersList };*/
    // case SET_IS_ROUND_RUN:
    //   return { ...state, game: { ...state.game, isRoundRun: value } };
    // case SET_CURRENT_ISSUE_ID:
    //   return { ...state, game: { ...state.game, currentIssueId: value } };
    // case SET_RESULT_CARD_VALUE:
    //   const newStatistic = JSON.parse(JSON.stringify(state.statistic));
    //   if (
    //     !newStatistic.some(
    //       (statisticItem: IStatistic) => statisticItem.issueId === state.game.currentIssueId,
    //     )
    //   ) {
    //     newStatistic.push({ issueId: state.game.currentIssueId, votes: [] });
    //   }
    //   newStatistic.map((statisticItem: IStatistic) => {
    //     if (statisticItem.issueId === state.game.currentIssueId) {
    //       const indexVote = statisticItem.votes.findIndex((vote) => vote.userId === me.id);
    //       if (indexVote === -1) {
    //         statisticItem.votes.push({ userId: me.id, valueCard: value });
    //       } else {
    //         statisticItem.votes[indexVote].valueCard = value;
    //       }
    //     }
    //   });
    //   return { ...state, statistic: newStatistic };
    default:
      return state;
  }
};
