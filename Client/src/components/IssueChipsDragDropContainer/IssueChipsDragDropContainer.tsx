import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DragDropContext, Droppable, Draggable, DropResult } from 'react-beautiful-dnd';

import { IssueChips } from '../IssueChips/IssueChips';
import { IssueChipsPlay } from '../IssueChipsPlay/IssueChipsPlay';

import { selectIssues } from '../../redux/selectors';
import { setNewOrderIssues } from '../../redux/actions';

import { IIssue } from '../../shared/types';

import './style.scss';

const reorder = (listIssue: IIssue[], startIndex: number, endIndex: number) => {
  const result = Array.from(listIssue);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

function Issue({
  issue,
  index,
  isDealer,
}: {
  issue: IIssue;
  index: number;
  isDealer: boolean;
}): JSX.Element {
  const { id, title, priority, description } = issue;
  return (
    <Draggable draggableId={id} index={index} isDragDisabled={!isDealer}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          className="issue-container-drag"
        >
          <IssueChipsPlay issueId={id} key={id}>
            <IssueChips id={id} title={title} priority={priority} description={description} />
          </IssueChipsPlay>
        </div>
      )}
    </Draggable>
  );
}

export function IssueChipsDragDropContainer({ isDealer }: { isDealer: boolean }): JSX.Element {
  const stateIssues = useSelector(selectIssues);
  const dispatch = useDispatch();

  function onDragEnd(result: DropResult) {
    if (!result.destination) return;
    if (result.destination.index === result.source.index) return;
    const newIssues = reorder(stateIssues, result.source.index, result.destination.index);

    dispatch(setNewOrderIssues(newIssues));
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="list">
        {(provided) => (
          <div
            ref={provided.innerRef}
            {...provided.droppableProps}
            className="issues-container-drag"
          >
            {stateIssues.length ? (
              stateIssues.map((issue: IIssue, index: number) => (
                <Issue issue={issue} index={index} key={issue.id} isDealer={isDealer} />
              ))
            ) : (
              <h3>No issues yet...</h3>
            )}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}
