import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Form, Input, Typography } from 'antd';
import { CloseOutlined, InfoCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';

import { checkValueLength } from '../../../shared/utils';

import { addCardValue } from '../../../redux/actions';

import './style.scss';

const { Text } = Typography;

export const CardAdd = (): JSX.Element => {
  const [inputValue, setInputValue] = useState('');
  const [isClickPlus, setIsClickPlus] = useState(false);
  const [isClickInfo, setIsClickInfo] = useState(false);

  const promptMessage = `You can add values ​​by one or as an array, separated by the "space". For example: 1 2 3...`;

  const dispatch = useDispatch();

  const handleClickAdd = () => {
    inputValue && dispatch(addCardValue(inputValue.split(' ')));
    inputValue && setInputValue('');
    inputValue && setIsClickPlus(!isClickPlus);
    inputValue && setIsClickInfo(false);
    !inputValue && setIsClickInfo(true);
  };

  return (
    <div className={`card-add${isClickPlus ? ' _flipped' : ''}`}>
      <div className="card-add__front">
        <PlusCircleOutlined className="card-add__add-icon" onClick={() => setIsClickPlus(true)} />
      </div>
      <div className="card-add__back">
        <div className="card-add__back-header">
          <InfoCircleOutlined
            className="card-add__back-header-info"
            onClick={() => setIsClickInfo(!isClickInfo)}
          />
          <CloseOutlined
            className="card-add__back-header-close"
            onClick={() => setIsClickPlus(false)}
          />
        </div>
        <Form className="card-add__form">
          <Input
            className="card-add__input"
            placeholder="Value"
            value={inputValue}
            onChange={({ target }) => setInputValue(checkValueLength(target.value))}
          />
          <Text className={`card-add__prompt ${isClickInfo && '_active'}`}>{promptMessage}</Text>
          <Button
            className="card-add__btn-add"
            type="primary"
            htmlType="submit"
            shape="round"
            size="small"
            onClick={handleClickAdd}
          >
            <PlusCircleOutlined className="card-add__btn-add-icon" />
          </Button>
        </Form>
      </div>
    </div>
  );
};
