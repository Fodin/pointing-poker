import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Typography } from 'antd';
import { CloseOutlined } from '@ant-design/icons';

import { checkValueSpace } from '../../shared/utils';
import { CardValue } from '../../shared/enums';
import { ICard } from '../../shared/types';

import { removeCardValue, changeCardValue } from '../../redux/actions';

import CoffeeSVG from '../../assets/coffee.svg';

import './style.scss';

const { Paragraph } = Typography;

const { COFFEE, QUESTION, INFINITY } = CardValue;

export const GameCard: React.FC<{
  isEditable?: boolean;
  card?: ICard;
  scale?: number;
}> = ({ isEditable, card, scale = 1 }) => {
  const [cardValue, setCardValue] = useState(card?.value);
  const [cardType, setCardType] = useState(card?.logo);
  const fraction1_2 = 0.5;

  const dispatch = useDispatch();

  const styles = {
    gameCard: {
      width: 120 * scale,
      height: 190 * scale,
      fontSize: `${14 * scale}px`,
    },
    coffeeIcon: {
      width: '18px',
      height: '18px',
      margin: 'auto',
    },
  };

  const getCardValue = (): string | number | undefined | JSX.Element => {
    if (Number(cardValue) === fraction1_2) return '½';
    if (cardValue === COFFEE) return <CoffeeSVG style={styles.coffeeIcon} viewBox="0 0 512 512" />;
    if (cardValue !== undefined) return cardValue;
    return '';
  };

  useEffect(() => {
    setCardValue(card?.value);
  }, [card?.value]);

  useEffect(() => {
    setCardType(card?.logo);
  }, [card?.logo]);

  const handleEditInput = (value: string) => {
    setCardValue(value);
    dispatch(changeCardValue({ oldValue: card?.value, newValue: value }));
  };

  return (
    <div className="game-card" style={styles.gameCard}>
      <div className="game-card__card-value-up">
        <div className="game-card__card-value">
          <Paragraph
            editable={
              isEditable && {
                onChange: (value) => value.length && handleEditInput(checkValueSpace(value)),
                maxLength: 3,
              }
            }
          >
            {getCardValue()}
          </Paragraph>
          {card?.value !== COFFEE && card?.value !== QUESTION && card?.value !== INFINITY && (
            <Paragraph>{cardType}</Paragraph>
          )}
        </div>
      </div>
      <div className="game-card__logo-field">
        {card?.value === COFFEE ? (
          <CoffeeSVG className="game-card__logo-svg" viewBox="0 0 512 512" />
        ) : card?.value === QUESTION ? (
          <p className="game-card__logo">{QUESTION}</p>
        ) : card?.value === INFINITY ? (
          <p className="game-card__logo">{INFINITY}</p>
        ) : (
          <p className="game-card__logo">{getCardValue()}</p>
        )}
      </div>
      <div className="game-card__card-value-down">
        <div className="game-card__card-value">
          {card?.value !== COFFEE && card?.value !== QUESTION && card?.value !== INFINITY && (
            <p className="game-card__inverted-value">{cardType}</p>
          )}
          <p className="game-card__inverted-value">{getCardValue()}</p>
        </div>
        {isEditable && (
          <CloseOutlined
            className="game-card__delete"
            onClick={() => dispatch(removeCardValue(card?.value))}
          />
        )}
      </div>
    </div>
  );
};
