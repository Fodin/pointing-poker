import React from 'react';
import './style.scss';
import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { selectIssues, selectSettings, selectStatistic } from '../../redux/selectors';
import { useSelector } from 'react-redux';
import { RecultItem } from '../RecultItem/RecultItem';
import { Button } from 'antd';
import {
  getAverageResult,
  getPercentForEachCardOfIssue,
  saveFileAsCSV,
  saveFileAsJson,
  saveFileAsTxt,
} from '../../shared/utils';
import { CardValue } from '../../shared/enums';
import { IIssueResult } from '../../shared/types';
const { COFFEE, QUESTION, INFINITY } = CardValue;

export const ResultPage = (): JSX.Element => {
  const issues = useSelector(selectIssues);
  const statistic = useSelector(selectStatistic);
  const { scoreType } = useSelector(selectSettings);

  const copyIssues = issues.slice() as IIssueResult[];

  copyIssues.forEach((issue) => {
    const resultIssue = getPercentForEachCardOfIssue(statistic, issue.id);
    issue.averageResult = getAverageResult(resultIssue, scoreType);

    // get result
    const newResultIssue: string[] = [];
    resultIssue
      .sort((a, b) => +b[1] - +a[1])
      .forEach((item) => {
        newResultIssue.push(
          `${item[0]} ${
            item[0] !== COFFEE && item[0] !== QUESTION && item[0] !== INFINITY ? scoreType : ''
          } - ${item[1]}%`,
        );
      });
    issue.result = newResultIssue.length ? newResultIssue.join(', ') : 'no result';
  });

  const handleSaveCSV = () => {
    saveFileAsCSV(copyIssues);
  };

  const handleSaveJson = () => {
    saveFileAsJson(copyIssues);
  };

  const handleSaveTxt = () => {
    saveFileAsTxt(copyIssues);
  };

  return (
    <div className="result-page">
      <Header />
      <main className="result-page__main">
        <div className="result-page__bnt-containet">
          <Button
            className="result-page__btn"
            type="primary"
            htmlType="button"
            shape="round"
            size="large"
            onClick={handleSaveCSV}
          >
            Save to csv
          </Button>
          <Button
            className="result-page__btn"
            type="primary"
            htmlType="button"
            shape="round"
            size="large"
            onClick={handleSaveJson}
          >
            Save to json
          </Button>
          <Button
            className="result-page__btn"
            type="primary"
            htmlType="button"
            shape="round"
            size="large"
            onClick={handleSaveTxt}
          >
            Save to txt
          </Button>
        </div>
        <div className="result-page__container">
          {copyIssues.map((issue) => {
            return <RecultItem issue={issue} key={issue.id} />;
          })}
        </div>
      </main>
      <Footer />
    </div>
  );
};
