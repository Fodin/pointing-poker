import React from 'react';
import { Typography } from 'antd';
import { IssueAddChips } from '../IssueChips/IssueAddChips/IssueAddChips';
import { useSelector } from 'react-redux';

import { IssueChipsDragDropContainer } from '../IssueChipsDragDropContainer/IssueChipsDragDropContainer';
import { me } from '../../shared/localData';
import { UserRole } from '../../shared/enums';

import { selectIssues } from '../../redux/selectors';

import './style.scss';

const { Dealer } = UserRole;

const { Title } = Typography;

const styles = {
  title: {
    marginBottom: '15px',
  },
};

export const IssuesSection = (): JSX.Element => {
  const issues = useSelector(selectIssues);

  return (
    <div className="issues-section section-field">
      <Title level={4} style={styles.title}>
        Issues{` (${issues.length})`}:
      </Title>
      <IssueChipsDragDropContainer isDealer={me.role === Dealer} />
      {me.role === Dealer && <IssueAddChips />}
    </div>
  );
};
