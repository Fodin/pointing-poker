import React from 'react';

import logo from '../../assets/logo.png';

import './style.scss';

export const Logo = (): JSX.Element => {
  return <img className="logo" src={logo} alt="logo" />;
};
