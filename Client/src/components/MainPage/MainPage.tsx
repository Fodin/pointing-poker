import React, { useState, useCallback } from 'react';
import { Header } from '../Header/Header';
import './style.scss';
import mainImage from '../../assets/poker-pointing.svg';
import { Button, Input } from 'antd';
import { Footer } from '../Footer/Footer';
import { LobbyConnectForm } from '../LobbyConnectForm/LobbyConnectForm';
import { ModalLayout } from '../ModalLayout/ModalLayout';

export const MainPage = (): JSX.Element => {
  const [urlGame, setUrlGame] = useState<string>('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isMaster, setIsMaster] = useState(false);

  const handleStartBtn = useCallback(() => {
    setIsMaster(true);
    setIsModalVisible(true);
  }, []);

  const handleConnectBtn = useCallback(() => {
    setIsMaster(false);
    setIsModalVisible(true);
  }, []);

  return (
    <div className="main-page">
      <Header />
      <main className="main-page__wrapper">
        <img className="main-page__img" src={`${mainImage}`} alt="poker-pointing" />
        <h3 className="main-page__title"> Start your planning:</h3>
        <div className="main-page__item">
          <div>Create session:</div>
          <Button
            className="main-page__btn"
            type="primary"
            htmlType="button"
            shape="round"
            size={'large'}
            onClick={handleStartBtn}
          >
            Start new game
          </Button>
        </div>
        <h3 className="main-page__title">OR:</h3>
        <div>Connect to lobby by URL:</div>
        <div className="main-page__item">
          <Input
            className="main-page__input"
            value={urlGame}
            onChange={(e) => setUrlGame(e.target.value)}
          />
          <Button
            className="main-page__btn"
            type="primary"
            htmlType="button"
            shape="round"
            size="large"
            onClick={handleConnectBtn}
          >
            Connect
          </Button>
        </div>
      </main>
      <ModalLayout
        isModalVisible={isModalVisible}
        onCloseForm={() => {
          setIsModalVisible(false);
        }}
      >
        <LobbyConnectForm />
      </ModalLayout>

      <Footer />
    </div>
  );
};
