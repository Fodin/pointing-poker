import React from 'react';
import { Button, message, Tooltip, Typography } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { CopyOutlined } from '@ant-design/icons';

import { SpringPlaning } from '../SpringPlaning/SpringPlaning';

import { setCurrentPage } from '../../redux/actions';
import { selectIssues } from '../../redux/selectors';

import { me } from '../../shared/localData';
import { UserRole } from '../../shared/enums';

import './style.scss';

const { Text } = Typography;
const { Dealer } = UserRole;

const styles = {
  btn: {
    width: '150px',
    marginTop: 40,
  },
};

const colorWhite = '#ffffff';

export const AdminSection = (): JSX.Element => {
  const issues = useSelector(selectIssues);

  const dispatch = useDispatch();

  const onStart = () => {
    dispatch(setCurrentPage(2));
  };

  const onCopyLink = () => {
    message.success('Link copied!');
    navigator.clipboard.writeText(location.href);
  };

  return (
    <div className="admin-section section-field">
      <SpringPlaning issues={issues} />
      {me.role === Dealer && (
        <>
          <div className="admin-section__lobby-link">
            <Tooltip
              title="Copy link"
              placement="top"
              zIndex={1}
              overlayInnerStyle={{ color: 'black' }}
              color={colorWhite}
            >
              <Button
                className="admin-section__lobby-link-btn"
                type="primary"
                htmlType="submit"
                shape="round"
                size="large"
                onClick={onCopyLink}
              >
                <CopyOutlined />
              </Button>
            </Tooltip>
            <Text className="admin-section__lobby-link-text">
              Copy the room link and send it to invite other team members
            </Text>
          </div>
        </>
      )}

      <div className="admin-section__btns">
        {me.role === Dealer && (
          <Tooltip
            visible={!issues.length}
            title="Create an issue to start the game."
            placement="leftBottom"
            zIndex={1}
            overlayInnerStyle={{ color: 'red' }}
            color={colorWhite}
            key={colorWhite}
          >
            <Button
              style={styles.btn}
              type="primary"
              htmlType="submit"
              shape="round"
              size="large"
              onClick={onStart}
              disabled={!issues.length}
            >
              Start game
            </Button>
          </Tooltip>
        )}
      </div>
    </div>
  );
};
