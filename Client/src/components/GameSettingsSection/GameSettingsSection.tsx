import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input, InputNumber, Select, Switch, Tooltip, Typography } from 'antd';

import { CardAdd } from '../GameCard/CardAdd/CardAdd';
import { GameCard } from '../GameCard/GameCard';

import { me } from '../../shared/localData';

import {
  CardSet,
  CardSetArray,
  CardValue,
  ScoreType,
  ScoreTypeName,
  UserRole,
} from '../../shared/enums';

import { Settings } from '../../domain/settings';
import { selectCardArray, selectSettings } from '../../redux/selectors';
import {
  setScoreType,
  setIsSeeOthersVotes,
  setIsTimerNeeded,
  setIsDealerVotes,
  setCardSet,
  setRoundTime,
} from '../../redux/actions';

import './style.scss';

const { Title } = Typography;
const { Item } = Form;
const { Option } = Select;

const { SP_NAME, TSHIRT_NAME, HOURS_NAME, DAYS_NAME, OTHER_NAME } = ScoreTypeName;
const { SP, TSHIRT, HOURS, DAYS, CUSTOM } = ScoreType;
const { FIBONACCI, M_FIBONACCI, TSHIRTS, POWERS_2, OTHER } = CardSet;
const { FIBONACCI_ARRAY, M_FIBONACCI_ARRAY, TSHIRTS_ARRAY, POWERS_2_ARRAY, OTHER_ARRAY } =
  CardSetArray;
const { COFFEE, QUESTION, INFINITY } = CardValue;
const { Dealer } = UserRole;

const maxScoreTypeLength = 2;
const colorWhite = '#ffffff';

const settingsLocal: Settings = {
  isDealerVotes: false,
  isSeeOthersVotes: false,
  isTimerNeeded: false,
  roundTime: 5,
  scoreType: 'SP',
  cardSet: ['Coffee', '?', '∞', '0', '1', '2', '3', '5', '8', '13', '21', '34', '55', '89'],
};

export const GameSettingsSection = (): JSX.Element => {
  const settings = useSelector(selectSettings);
  const [isDealerVotesState, setIsDealerVotesState] = useState(settings.isDealerVotes);
  const [isSeeOthersVotesState, setIsSeeOthersVotesState] = useState(settings.isSeeOthersVotes);
  const [isTimerNeededState, setIsTimerNeededState] = useState(settings.isTimerNeeded);
  const [cardType, setCardType] = useState(SP);
  const [customScoreType, setCustomScoreType] = useState('');
  const [cardSetInput, setCardSetInput] = useState(FIBONACCI_ARRAY);

  const dispatch = useDispatch();

  const { isTimerNeeded, roundTime, scoreType, cardSet } = useSelector(selectSettings);
  const cardArray = useSelector(selectCardArray);

  useEffect(() => {
    setIsDealerVotesState(settings.isDealerVotes);
    setIsSeeOthersVotesState(settings.isSeeOthersVotes);
    setIsTimerNeededState(settings.isTimerNeeded);
    switch (scoreType) {
      case SP:
        setCardType(SP);
        break;
      case TSHIRT:
        setCardType(TSHIRT);
        break;
      case HOURS:
        setCardType(HOURS);
        break;
      case DAYS:
        setCardType(DAYS);
        break;
      default:
        setCardType(CUSTOM);
        setCustomScoreType(scoreType);
        break;
    }
    const localStorageCardSet = localStorage.getItem('cardSet');
    if (localStorageCardSet) {
      setCardSetInput(JSON.parse(localStorageCardSet));
    }
    if (JSON.stringify(settings) !== JSON.stringify(settingsLocal) && me.role === Dealer) {
      localStorage.setItem('settings', JSON.stringify(settings));
      localStorage.setItem('cardArray', JSON.stringify(cardArray));
    }
  }, [settings]);

  useEffect(() => {
    cardType === CUSTOM
      ? dispatch(setScoreType(customScoreType))
      : dispatch(setScoreType(cardType));
  }, [cardType, customScoreType, dispatch]);

  useEffect(() => {
    cardSetInput === OTHER_ARRAY
      ? dispatch(setCardSet(cardArray))
      : dispatch(setCardSet(cardSetInput.split(',')));
  }, [cardSetInput, cardArray, dispatch]);

  return (
    <div className="game-settings section-field">
      <Title level={4}>Game settings:</Title>
      <div className="game-settings__form">
        <div className="game-settings__container">
          <Item label="Dealer votes">
            <Switch
              checked={isDealerVotesState}
              onChange={(value) => dispatch(setIsDealerVotes(value))}
              disabled={me.role !== Dealer}
            />
          </Item>
          <Item label="Can see others votes">
            <Switch
              checked={isSeeOthersVotesState}
              onChange={(value) => dispatch(setIsSeeOthersVotes(value))}
              disabled={me.role !== Dealer}
            />
          </Item>
          <Item label="Timer">
            <Switch
              checked={isTimerNeededState}
              onChange={(value) => dispatch(setIsTimerNeeded(value))}
              disabled={me.role !== Dealer}
            />
          </Item>
          {isTimerNeeded && (
            <Item className="game-settings__number-input" label="Round time (seconds):">
              <Tooltip
                title="Set a value between 5 and 360 seconds"
                overlayInnerStyle={{ color: 'black' }}
                color={colorWhite}
                key={colorWhite}
              >
                <InputNumber
                  value={roundTime}
                  min={5}
                  max={360}
                  placeholder="sec"
                  onChange={(value) => dispatch(setRoundTime(value))}
                  disabled={me.role !== Dealer}
                />
              </Tooltip>
            </Item>
          )}
          {me.role === Dealer && (
            <Item label="Score type:">
              <Select value={cardType} onChange={(value) => setCardType(value)}>
                <Option value={SP}>{SP_NAME}</Option>
                <Option value={TSHIRT}>{TSHIRT_NAME}</Option>
                <Option value={HOURS}>{HOURS_NAME}</Option>
                <Option value={DAYS}>{DAYS_NAME}</Option>
                <Option value={CUSTOM}>{OTHER_NAME}</Option>
              </Select>
            </Item>
          )}
          {cardType === CUSTOM && me.role === Dealer && (
            <Item label="Custom score type:">
              <Input
                value={customScoreType}
                placeholder="Score type (max 2 length)"
                onChange={(event) =>
                  setCustomScoreType(event.target.value.slice(0, maxScoreTypeLength))
                }
              />
            </Item>
          )}
          {me.role === Dealer && (
            <Item label="Card set:">
              <Select
                value={cardSetInput}
                onChange={(value) => {
                  setCardSetInput(value);
                  localStorage.setItem('cardSet', JSON.stringify(value));
                }}
              >
                <Option value={FIBONACCI_ARRAY}>{FIBONACCI}</Option>
                <Option value={M_FIBONACCI_ARRAY}>{M_FIBONACCI}</Option>
                <Option value={TSHIRTS_ARRAY}>{TSHIRTS}</Option>
                <Option value={POWERS_2_ARRAY}>{POWERS_2}</Option>
                <Option value={OTHER_ARRAY}>{OTHER}</Option>
              </Select>
            </Item>
          )}
        </div>
        <label className="game-settings__cards-label">Card view:</label>
        <div className="game-settings__cards">
          {cardSet?.map((cardValue, idx) => {
            return (
              <GameCard
                isEditable={
                  cardSetInput === OTHER_ARRAY &&
                  cardValue !== COFFEE &&
                  cardValue !== QUESTION &&
                  cardValue !== INFINITY
                }
                card={{ id: idx, value: cardValue, logo: scoreType }}
                key={idx}
              />
            );
          })}
          {cardSetInput === OTHER_ARRAY && <CardAdd />}
        </div>
      </div>
    </div>
  );
};
