import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Form } from 'antd';
import { CloseOutlined, SendOutlined } from '@ant-design/icons';

import { ChatMessage as ChatMessageType } from '../../domain/chat';
import { ChatMessage } from './ChatMessage/ChatMessage';

import { chatSendMessage } from '../../redux/actions';
import { selectChat, selectUsers } from '../../redux/selectors';

import { IUser } from '../../shared/types';

import './style.scss';

const { TextArea } = Input;
const { Item } = Form;

const chatVisible = {
  top: '10vh',
  left: 20,
  transition: '0.6s',
};

const chatHidden = {
  top: '10vh',
  left: -500,
  transition: '0.6s',
};

export const Chat = ({
  isChatOpen,
  setIsChatOpen,
}: {
  isChatOpen: boolean;
  setIsChatOpen: React.Dispatch<React.SetStateAction<boolean>>;
}): JSX.Element => {
  const chat: ChatMessageType[] = useSelector(selectChat);

  const users = useSelector(selectUsers);

  const [chatPos, setChatPos] = useState<React.CSSProperties>({
    top: 0,
    left: -500,
    transition: 'none',
  });
  const [chatPosCurrent, setChatPosCurrent] = useState<React.CSSProperties>({
    top: 0,
    left: 0,
    transition: 'none',
  });
  const [isClick, setIsClick] = useState(false);

  const [form] = Form.useForm();

  const messageFieldRef = useRef<any>();
  const btnClickRef = useRef<any>();

  const dispatch = useDispatch();

  useEffect(() => {
    if (messageFieldRef.current) {
      messageFieldRef.current.scrollTop = messageFieldRef.current.scrollHeight;
    }
  }, [chat]);

  const onFinish = (values: any) => {
    values.message && dispatch(chatSendMessage(values.message));
    form.resetFields();
  };

  useEffect(() => {
    isChatOpen ? setChatPos(chatVisible) : setChatPos(chatHidden);
  }, [isChatOpen]);

  const styles = {
    chatFormClick: isChatOpen && isClick ? chatPosCurrent : chatPos,
  };

  const mouseDownHandler = useCallback((e) => {
    setIsClick(e.target.classList.contains('chat-form__btn-move'));
  }, []);

  const mouseUpHandler = useCallback(() => {
    setChatPos(chatPosCurrent);
    setIsClick(false);
  }, [chatPosCurrent]);

  const mousemoveHandler = useCallback((e) => {
    setChatPosCurrent({ top: e.pageY - 14, left: e.pageX - 19, transition: 'none' });
  }, []);

  useEffect(() => {
    window.addEventListener('mousemove', mousemoveHandler);
    return () => {
      window.removeEventListener('mousemove', mousemoveHandler);
    };
  }, [mousemoveHandler]);

  const getUser = (userId: string): IUser | undefined => {
    return users.find((user) => user.id === userId);
  };

  const handleEnterClick = useCallback((e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      btnClickRef.current?.click();
    }
  }, []);

  return (
    <div className="chat-form" style={styles.chatFormClick}>
      <div className="chat-form__header">
        <div
          className="chat-form__btn-move"
          onMouseDown={mouseDownHandler}
          onMouseUp={mouseUpHandler}
        >
          &harr;
        </div>
        <CloseOutlined className="chat-form__btn-close" onClick={() => setIsChatOpen(false)} />
      </div>
      <div ref={messageFieldRef} className="chat-form__message-field">
        {chat.map((message) => {
          return (
            <ChatMessage
              key={message.id}
              user={getUser(message.userId)}
              message={message.message}
            />
          );
        })}
      </div>
      <Form className="chat-form__form" form={form} onFinish={onFinish}>
        <Item name="message" className="chat-form__textarea-container">
          <TextArea
            className="chat-form__textarea"
            placeholder="Message"
            autoSize={{ minRows: 1, maxRows: 5 }}
            maxLength={1000}
            onKeyPress={handleEnterClick}
            autoFocus
          />
        </Item>
        <div className="chat-form__btn-field">
          <button ref={btnClickRef} type="submit" className="chat-form__btn-send">
            <SendOutlined />
          </button>
        </div>
      </Form>
    </div>
  );
};
