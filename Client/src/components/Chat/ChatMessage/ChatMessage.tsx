import React from 'react';
import { Avatar } from 'antd';

import { getColor, getFirstCharsOfWords } from '../../../shared/utils';

import { IUser } from '../../../shared/types';

import './style';

export const ChatMessage = ({ user, message }: { user?: IUser; message: string }): JSX.Element => {
  return (
    <div className="chat-message">
      <div className="chat-message__profile">
        <div className="chat-message__avatar">
          <Avatar
            size={40}
            src={user?.avatar}
            style={{
              backgroundColor: getColor(user?.id + ''),
            }}
          >
            {!user?.avatar && getFirstCharsOfWords(user?.firstName, user?.lastName)}
          </Avatar>
        </div>
        <div className="chat-message__info">
          <span className="chat-message__name">{`${user?.firstName} ${
            user?.lastName ? user.lastName : ''
          }`}</span>
          {user?.position && <span className="chat-message__position">{user?.position}</span>}
        </div>
      </div>
      <span className="chat-message__message">{message}</span>
    </div>
  );
};
