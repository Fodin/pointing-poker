import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Typography } from 'antd';

import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { SpringPlaning } from '../SpringPlaning/SpringPlaning';
import { GameCardsField } from '../GameCardsField/GameCardsField';
import { VotingField } from '../VotingField/VotingField';
import { ControlField } from '../ControlField/ControlField';
import { Statistic } from '../Statistic/Statistic';
import { IssuesSection } from '../IssuesSection/IssuesSection';

import { setCurrentIssueId } from '../../redux/actions';
import {
  selectIssues,
  selectSettings,
  selectIsRoundRun,
  selectCurrentIssueId,
  selectStatistic,
} from '../../redux/selectors';

import { me } from '../../shared/localData';
import { UserRole } from '../../shared/enums';
import { getAverageResult, getPercentForEachCardOfIssue } from '../../shared/utils';

import './style.scss';

const { Observer, Dealer, Player } = UserRole;
const { Title, Text } = Typography;

export const Gamepage = (): JSX.Element => {
  const isRoundRun = useSelector(selectIsRoundRun);
  const issues = useSelector(selectIssues);
  const settings = useSelector(selectSettings);
  const currentIssueId = useSelector(selectCurrentIssueId);
  const statistic = useSelector(selectStatistic);
  const { scoreType } = useSelector(selectSettings);

  const currentIssue = issues.find(({ id }) => id === currentIssueId);

  const dispatch = useDispatch();

  const resultIssue = getPercentForEachCardOfIssue(statistic, currentIssueId);

  const averageResult = useMemo(() => {
    return getAverageResult(resultIssue, scoreType);
  }, [isRoundRun, currentIssueId]);

  useEffect(() => {
    dispatch(setCurrentIssueId(issues[0].id));
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="game-page">
      <Header />
      <main className="game-page__main">
        <SpringPlaning issues={issues} />
        <div className="game-page__container">
          <IssuesSection />
          <ControlField isMaster={me.role === Dealer} />
          <div className="game-page__current-issue section-field">
            <Title level={4}>Current issue:</Title>
            <Title level={5}>{currentIssue?.title || 'No issues yet...'}</Title>
            <Text>{currentIssue?.description}</Text>
          </div>
          <VotingField />
        </div>
        {isRoundRun && me.role !== Observer && (settings.isDealerVotes || me.role == Player) && (
          <GameCardsField />
        )}
        {!isRoundRun && (
          <div className="game-page__result section-field">
            <Title level={3}>{`Result: ${averageResult}`}</Title>
            <Statistic issueId={currentIssueId} />
          </div>
        )}
      </main>
      <Footer />
    </div>
  );
};
