import React from 'react';
import { ICard } from '../../shared/types';
import './style.scss';
import { GameCard } from '../GameCard/GameCard';

interface IGameCardPlay {
  card: ICard;
  absoluteLeft: number;
  isCurrent: boolean;
  onclick: (value: string | number) => void;
}

export const GameCardPlay = ({
  card,
  absoluteLeft,
  isCurrent,
  onclick,
}: IGameCardPlay): JSX.Element => {
  return (
    <div
      className={`game-card-play ${isCurrent && 'game-card-play__current'}`}
      style={{ left: absoluteLeft }}
      onClick={() => {
        onclick(card.value);
      }}
    >
      <GameCard card={card} isEditable={false} />
    </div>
  );
};
