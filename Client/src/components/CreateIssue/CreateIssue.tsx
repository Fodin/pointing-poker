import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Form, Input, Select, Switch, Tooltip, Typography, Upload } from 'antd';
import { ClearOutlined, UploadOutlined } from '@ant-design/icons';
import { nanoid } from 'nanoid';

import { PriorityIssue } from '../../shared/enums';
import { getIssuesFromPC } from '../../shared/utils';

import { addNewIssue } from '../../redux/actions';

import './style.scss';

const { Item } = Form;
const { TextArea } = Input;
const { Option } = Select;
const { Title } = Typography;

const { LOW, MIDDLE, HIGHT } = PriorityIssue;

const styles = {
  title: {
    marginBottom: 30,
  },
  input: {
    borderRadius: 20,
  },
  btn: {
    width: 100,
  },
};

export const CreateIssue: React.FC<{ onCloseForm: () => void }> = ({
  onCloseForm,
}): JSX.Element => {
  const [isFileLoad, setIsFileLoad] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [priority, setPriority] = useState(LOW);
  const [issueFromFile, setIssueFromFile] = useState<string | ArrayBuffer | null>('');
  const [issueArr, setIssueArr] = useState<string[]>([]);
  const [modalIsOpen, setModalIsOpen] = useState(true);

  const [form] = Form.useForm();

  const dispatch = useDispatch();

  const cleanForm = (): void => {
    form.resetFields();
    setPriority(LOW);
    setIssueFromFile('');
  };

  const onSubmit = () => {
    !isFileLoad
      ? dispatch(
          addNewIssue({
            id: nanoid(),
            title,
            description,
            priority,
          }),
        )
      : issueArr.map((data) => {
          const title = data.split('\n')[0];
          const description = data.split('\n')[1];
          dispatch(
            addNewIssue({
              id: nanoid(),
              title,
              description,
              priority: MIDDLE,
            }),
          );
        });
    onCloseForm();
    cleanForm();
  };

  useEffect(() => {
    const issueArrTxt = String(issueFromFile)?.split('\n\n');
    setIssueArr(issueArrTxt);
  }, [issueFromFile]);

  useEffect(() => {
    setModalIsOpen(true);
    return () => {
      setModalIsOpen(false);
    };
  }, [modalIsOpen]);

  return (
    <div className="create-issue">
      <Title style={styles.title} level={2}>
        Create Issue
      </Title>
      <div className="create-issue__file-load">
        <label>Load issues from PC in .txt:</label>
        <Switch checked={isFileLoad} onChange={(value) => setIsFileLoad(value)} />
      </div>
      <Form onFinish={onSubmit} form={form}>
        {!isFileLoad ? (
          <>
            <Item
              name="Title"
              label="Title:"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input
                style={styles.input}
                placeholder="Title (max length 90 characters)"
                maxLength={90}
                value={title}
                onChange={({ target }) => setTitle(target.value)}
              />
            </Item>

            <Item name="description" label="Description:">
              <TextArea
                className="create-issue__textarea"
                name="description"
                placeholder="Description (max length 1000 characters)"
                autoSize={{ minRows: 6, maxRows: 6 }}
                maxLength={1000}
                value={description}
                onChange={({ target }) => setDescription(target.value)}
              />
            </Item>

            <Item label="Priority:">
              <Select
                dropdownStyle={styles.input}
                value={priority}
                onChange={(value) => setPriority(value)}
              >
                <Option value={LOW}>{LOW}</Option>
                <Option value={MIDDLE}>{MIDDLE}</Option>
                <Option value={HIGHT}>{HIGHT}</Option>
              </Select>
            </Item>
          </>
        ) : (
          <>
            <div className="create-issue__file-load">
              <label>
                <span style={{ color: 'red' }}>* </span>Upload file from PC:
              </label>
              <Upload
                accept=".txt"
                showUploadList={false}
                onChange={({ file }) =>
                  getIssuesFromPC(file as { originFileObj: Blob }, setIssueFromFile)
                }
              >
                <Button
                  style={{ width: '110px', borderRadius: '14px' }}
                  size="middle"
                  icon={<UploadOutlined />}
                >
                  Upload
                </Button>
              </Upload>
            </div>
            <div className="create-issue__file-load">
              <label>File contents:</label>
              <pre
                className="create-issue__file-load-view"
                style={{ marginBottom: '0', color: issueFromFile ? 'black' : 'red' }}
              >
                {!issueFromFile
                  ? `The title and description must be
on separate lines. New issue should
be separated by a blank line.
For example:

  Title #1
  Description #1

  Title #2
  Description #2

  Title #3
  Description #3`
                  : issueFromFile}
              </pre>
            </div>
          </>
        )}

        {isFileLoad && issueFromFile !== '' && (
          <div className="create-issue__file-load uploaded">
            <label>Uploaded issues:</label>
            <p style={{ color: 'red' }}>{`Add ${issueArr.length} issue${
              issueArr.length > 1 ? 's' : ''
            }?`}</p>
          </div>
        )}

        <div className="create-issue__btns" style={{ marginTop: isFileLoad ? '0' : '40px' }}>
          <Tooltip
            trigger={`${isFileLoad && issueFromFile === '' && 'hover'}`}
            title="Upload the file"
            placement="left"
            overlayInnerStyle={{ color: 'red' }}
            color="white"
          >
            <Button
              disabled={isFileLoad && issueFromFile === ''}
              style={styles.btn}
              type="primary"
              htmlType="submit"
              shape="round"
              size="large"
            >
              Yes
            </Button>
          </Tooltip>
          <Button
            style={styles.btn}
            type="default"
            htmlType="button"
            shape="round"
            size="large"
            danger
            onClick={() => onCloseForm()}
          >
            No
          </Button>
          <Tooltip title="Clear field" overlayInnerStyle={{ color: 'black' }} color="white">
            <Button
              icon={<ClearOutlined />}
              type="dashed"
              htmlType="button"
              shape="round"
              size="large"
              onClick={cleanForm}
            />
          </Tooltip>
        </div>
      </Form>
    </div>
  );
};
