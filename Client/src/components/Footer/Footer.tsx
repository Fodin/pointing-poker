import React from 'react';

import LogoGit from '../../assets/git.svg';
import LogoRss from '../../assets/logo-rss.svg';

import './style.scss';

export const Footer = (): JSX.Element => {
  return (
    <footer className="footer">
      <div className="footer-content">
        <div className="footer__github">
          <LogoGit />
          <ul className="footer__github-list">
            <li>
              <a href="https://github.com/Fodin">Fodin</a>
            </li>
            <li>
              <a href="https://github.com/yuri-9">Yuri-9</a>
            </li>
            <li>
              <a href="https://github.com/prig25">Prig25</a>
            </li>
          </ul>
        </div>
        <a href="https://rs.school/js/" className="footer__rss">
          <LogoRss />
          <span className="footer__rss-year">&apos;2021</span>
        </a>
      </div>
    </footer>
  );
};
