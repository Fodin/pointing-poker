import React from 'react';
import { Typography } from 'antd';
import { useSelector } from 'react-redux';

import { selectIsRoundRun, selectSettings, selectStatistic } from '../../redux/selectors';
import { getPercentForEachCardOfIssue } from '../../shared/utils';

import { GameCard } from '../GameCard/GameCard';

import './style.scss';

const { Text } = Typography;

export const Statistic = ({ issueId }: { issueId: string }): JSX.Element => {
  const statistic = useSelector(selectStatistic);
  const isRoundRun = useSelector(selectIsRoundRun);
  const { scoreType } = useSelector(selectSettings);

  const resultStatisticArray = getPercentForEachCardOfIssue(statistic, issueId);

  return (
    <div className="statistic">
      {!isRoundRun
        ? resultStatisticArray
            .sort((a, b) => +b[1] - +a[1])
            .map((item) => (
              <div key={item[0]} className="statistic__item">
                <GameCard
                  card={{ id: 1, value: item[0], logo: scoreType }}
                  scale={0.7}
                  isEditable={false}
                />
                <span>{`${item[1]}%`}</span>
              </div>
            ))
        : null}
      {!resultStatisticArray.length && <Text>No statistic yet...</Text>}
    </div>
  );
};
