import React from 'react';
import { useSelector } from 'react-redux';

import { LobbyConnectForm } from './LobbyConnectForm/LobbyConnectForm';
import { Gamepage } from './GamePage/GamePage';
import { LobbyPage } from './LobbyPage/LobbyPage';
import { selectGame } from '../redux/selectors';
import { ResultPage } from './ResultPage/ResultPage';
import { FatalError } from './FatalError/FatalError';

export const App = (): JSX.Element => {
  const game = useSelector(selectGame);
  const lobbyConnectForm = 0;
  const lobbyPage = 1;
  const gamePage = 2;
  const resultPage = 3;
  const fatalErrorPage = 4;

  const pages = [
    <LobbyConnectForm key={lobbyConnectForm} />,
    <LobbyPage key={lobbyPage} />,
    <Gamepage key={gamePage} />,
    <ResultPage key={resultPage} />,
    <FatalError key={fatalErrorPage} />,
  ];

  return pages[game.currentPage || lobbyConnectForm];
};
