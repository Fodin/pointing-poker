import React from 'react';
import { useSelector } from 'react-redux';
import { Typography } from 'antd';

import { IIssue } from '../../shared/types';

import { selectGame } from '../../redux/selectors';

import './style.scss';

const { Paragraph } = Typography;

const gamePage = 2;

interface ISpringPlaning {
  issues: IIssue[];
}

export const SpringPlaning = ({ issues }: ISpringPlaning): JSX.Element => {
  const game = useSelector(selectGame);

  return (
    <Paragraph
      ellipsis
      className={`spring-planing ${game.currentPage === gamePage && 'section-field'}`}
    >
      {`Sprint ${issues.length} planning ${issues.length !== 0 ? '( issues:' : ''} ${issues
        .map((issue) => issue.title)
        .join(', ')} ${issues.length !== 0 ? ')' : ''}`}
    </Paragraph>
  );
};
