import React, { ChangeEvent, useState } from 'react';
import { Avatar, Button, Col, Form, Input, Row, Tooltip, Typography, Upload } from 'antd';
import { ClearOutlined, QuestionOutlined, UploadOutlined, UserOutlined } from '@ant-design/icons';
import ImgCrop from 'antd-img-crop';
import { UploadChangeParam } from 'antd/lib/upload';
import { UploadFile } from 'antd/lib/upload/interface';
import { nanoid } from '@reduxjs/toolkit';

import titleimg from '../../assets/title.png';
import { getFirstCharsOfWords, resizeImage } from '../../shared/utils';
import { connect } from '../../shared/socketio';
import Storage from '../../shared/storage';
import { IUser } from '../../shared/types';
import { UserRole } from '../../shared/enums';
import { setMyData } from '../../shared/localData';

import './style.scss';

const { Title } = Typography;

const styles = {
  title: {
    marginBottom: 40,
    textAlign: 'center' as const,
  },
  input: {
    borderRadius: 50,
  },
};

export const LobbyConnectForm = (): JSX.Element => {
  const defaultUserData = Storage.load('user');
  const [base64AvatarImage, setBase64AvatarImage] = useState<string | ArrayBuffer | null>(
    defaultUserData?.avatar || '',
  );
  const [userRole, setUserRole] = useState<UserRole>(UserRole.Player);
  const [initialsOfName, setInitialsOfName] = useState<string>('');
  const [form] = Form.useForm();

  const handleUploadFile = ({ file }: UploadChangeParam<UploadFile<string>>) => {
    resizeImage(file as { originFileObj: Blob }).then((res: string) => setBase64AvatarImage(res));
  };

  const handleClear = () => {
    Storage.remove('user');
    setBase64AvatarImage(null);
    form.setFieldsValue({
      firstName: '',
      lastName: '',
      jobPosition: '',
    });
    setInitialsOfName('');
  };

  const handleRandomAvatar = () => {
    const loadImage = async (url: string) => {
      const blob = await fetch(url).then((res) => res.blob());

      return new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsText(blob);
        reader.onload = () => {
          const base64data = reader.result;
          resolve(base64data);
        };
      });
    };

    loadImage(location.origin + '/avatar').then((res) => {
      setBase64AvatarImage(res as string);
    });
  };

  const handleClearAvatar = () => {
    setBase64AvatarImage(null);
  };

  const handleSubmit = async (e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log('submit');
  };

  const onFinish = (values: { firstName: string; lastName: string; jobPosition: string }) => {
    const gameId = userRole === UserRole.Dealer ? nanoid() : location.pathname.substring(1);
    const user: IUser = {
      firstName: values.firstName,
      lastName: values.lastName || '',
      position: values.jobPosition || '',
      role: userRole,
      id: defaultUserData ? defaultUserData.id : nanoid(),
      avatar: base64AvatarImage,
    };
    setMyData(user);
    Storage.save('user', user);
    history.replaceState(null, '', gameId);
    connect(gameId, user.id, user.role);
  };

  const isDealer = window.location.pathname === '/';

  const onValuesChange = () => {
    const firstName = form.getFieldValue('firstName');
    const lastName = form.getFieldValue('lastName');
    setInitialsOfName(getFirstCharsOfWords(firstName, lastName));
  };

  return (
    <div className="lobby-connect-wrapper">
      <img className="lobby-connect-title" src={titleimg} />
      <div className="lobby-connect-form">
        <Title style={styles.title} level={2}>
          {isDealer ? 'Create new game' : 'Connect to lobby'}
        </Title>
        <Form
          form={form}
          onSubmitCapture={handleSubmit}
          onFinish={onFinish}
          onValuesChange={onValuesChange}
        >
          <Form.Item
            name="firstName"
            label="First name:"
            rules={[
              {
                required: true,
              },
            ]}
            initialValue={defaultUserData?.firstName}
          >
            <Input maxLength={20} style={styles.input} />
          </Form.Item>
          <Form.Item name="lastName" label="Last name:" initialValue={defaultUserData?.lastName}>
            <Input maxLength={20} style={styles.input} />
          </Form.Item>
          <Form.Item
            name="jobPosition"
            label="Job position:"
            initialValue={defaultUserData?.position}
          >
            <Input maxLength={20} style={styles.input} />
          </Form.Item>
          <Form.Item label="Avatar:">
            <Row align="middle" gutter={8}>
              <Col>
                <Avatar
                  size={64}
                  src={base64AvatarImage}
                  className="lobby-connect-form__avatar"
                  icon={!initialsOfName && <UserOutlined />}
                >
                  {initialsOfName}
                </Avatar>
              </Col>
              <Col>
                <div className="lobby-connect-form__avatar_btns">
                  <ImgCrop rotate shape="round">
                    <Upload showUploadList={false} onChange={handleUploadFile}>
                      <Tooltip
                        placement="right"
                        title="Upload"
                        overlayInnerStyle={{ color: 'black' }}
                        trigger="hover"
                        color="#ffffff"
                      >
                        <Button
                          size="small"
                          icon={<UploadOutlined />}
                          shape="circle"
                          onClick={() => console.log('upload')}
                        />
                      </Tooltip>
                    </Upload>
                  </ImgCrop>
                  <Tooltip
                    placement="right"
                    title="Random"
                    overlayInnerStyle={{ color: 'black' }}
                    trigger="hover"
                    color="#ffffff"
                  >
                    <Button
                      size="small"
                      icon={<QuestionOutlined />}
                      shape="circle"
                      onClick={handleRandomAvatar}
                    />
                  </Tooltip>
                  <Tooltip
                    placement="right"
                    title="Clear"
                    overlayInnerStyle={{ color: 'black' }}
                    trigger="hover"
                    color="#ffffff"
                  >
                    <Button
                      size="small"
                      icon={<ClearOutlined />}
                      shape="circle"
                      onClick={handleClearAvatar}
                    />
                  </Tooltip>
                </div>
              </Col>
            </Row>
          </Form.Item>

          <div className="lobby-connect-form__btns">
            {isDealer ? (
              <Button
                type="primary"
                htmlType="submit"
                shape="round"
                size={'large'}
                onClick={() => setUserRole(UserRole.Dealer)}
              >
                Create game
              </Button>
            ) : (
              <>
                <Button
                  type="primary"
                  htmlType="submit"
                  shape="round"
                  size={'large'}
                  onClick={() => setUserRole(UserRole.Player)}
                >
                  Join
                </Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  shape="round"
                  size={'large'}
                  onClick={() => setUserRole(UserRole.Observer)}
                >
                  Observe
                </Button>
              </>
            )}
            <Button
              type="dashed"
              htmlType="button"
              shape="round"
              size={'large'}
              onClick={handleClear}
            >
              Clear
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};
