import { Col, Row } from 'antd';
import React from 'react';
import { PlayerCard } from '../PlayerCard/PlayerCard';
import './style.scss';
import { CardValue, UserRole } from '../../shared/enums';
import { useSelector } from 'react-redux';
import {
  selectCurrentIssueId,
  selectIsRoundRun,
  selectSettings,
  selectStatistic,
  selectUsers,
} from '../../redux/selectors';
import { me } from '../../shared/localData';
const { Observer, Dealer } = UserRole;
const { COFFEE, QUESTION, INFINITY } = CardValue;

export const VotingField = (): JSX.Element => {
  const users = useSelector(selectUsers);
  const statistic = useSelector(selectStatistic);
  const currentIssueId = useSelector(selectCurrentIssueId);
  const isRoundRun = useSelector(selectIsRoundRun);
  const settings = useSelector(selectSettings);

  const getIssueStatistic = (userId: string) => {
    const statisticUser = statistic
      .find((itemIssue) => itemIssue.issueId === currentIssueId)
      ?.votes.find((itemUser) => itemUser.userId === userId)?.valueCard;

    return statisticUser;
  };

  const getValue = (value?: string) => {
    if (INFINITY === value || QUESTION === value || COFFEE === value) {
      return value;
    }
    if (value) {
      return value + ' SP';
    }
    return '____';
  };

  return (
    <div className="voting section-field">
      <Row gutter={10} style={{ marginBottom: 10 }}>
        <Col span={8}>
          <div className="voting__title">Score:</div>
        </Col>
        <Col span={16}>
          <div className="voting__title">Players:</div>
        </Col>
      </Row>
      {users
        .filter((user) => user.role !== Observer)
        .map((user) => {
          const value = getIssueStatistic(user.id);
          return (
            <Row key={user.id} gutter={10} style={{ marginBottom: 10 }}>
              <Col
                span={8}
                className={`${
                  user.role === Dealer && !settings.isDealerVotes ? 'voting__card_hidden' : ''
                }`}
              >
                {!isRoundRun || user.id === me.id || settings.isSeeOthersVotes ? (
                  <div className={`voting__card ${value && isRoundRun ? 'die-cast' : null}`}>
                    {getValue(value)}
                  </div>
                ) : getIssueStatistic(user.id) ? (
                  <div className="voting__card die-cast">Voted</div>
                ) : (
                  <div className="voting__card">Thinking</div>
                )}
              </Col>
              <Col span={16}>
                <PlayerCard user={user} scale={0.8} />
              </Col>
            </Row>
          );
        })}
      {users.some((user) => user.role === Observer) && (
        <Row gutter={10} justify={'end'} style={{ marginBottom: 10 }}>
          <Col span={16}>
            <div className="voting__title">Observers:</div>
          </Col>
        </Row>
      )}
      {users
        .filter((user) => user.role === Observer)
        .map((user) => (
          <Row key={user.id} justify={'end'} gutter={10} style={{ marginBottom: 10 }}>
            <Col span={16}>
              <PlayerCard
                user={user}
                onDeletePlayer={() => {
                  console.log('delete');
                }}
                hasBtn={true}
                scale={0.8}
              />
            </Col>
          </Row>
        ))}
    </div>
  );
};
