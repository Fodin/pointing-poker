import React, { useState } from 'react';
import { Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import { ModalLayout } from '../../ModalLayout/ModalLayout';
import { CreateIssue } from '../../CreateIssue/CreateIssue';

import './style.scss';

const { Text } = Typography;

export const IssueAddChips = (): JSX.Element => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (
    <div className="issue-add issue-field">
      <div className="issue-add__content">
        <Text>Create new issue</Text>
        <PlusOutlined />
      </div>
      <div className="issue-add__clickable-field" onClick={() => setIsModalVisible(true)} />
      <ModalLayout
        isModalVisible={isModalVisible}
        onCloseForm={() => {
          setIsModalVisible(false);
        }}
      >
        <CreateIssue onCloseForm={() => setIsModalVisible(false)} />
      </ModalLayout>
    </div>
  );
};
