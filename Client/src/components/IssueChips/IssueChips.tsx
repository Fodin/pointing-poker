import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Popover, Tooltip, Typography } from 'antd';
import { DeleteOutlined, FormOutlined, InfoCircleOutlined } from '@ant-design/icons';

import { UserRole } from '../../shared/enums';
import { me } from '../../shared/localData';
import { IIssue } from '../../shared/types';

import { removeIssue, changeIssueTitle, changeIssueDescription } from '../../redux/actions';
import { selectCurrentIssueId, selectIsRoundRun } from '../../redux/selectors';

import './style.scss';

const { Paragraph, Text } = Typography;
const { Dealer } = UserRole;
const colorWhite = '#ffffff';

export const IssueChips = ({ id, title, priority, description }: IIssue): JSX.Element => {
  const [chipsTitle, setChipsTitle] = useState(title);
  const [chipsDescription, setChipsDescription] = useState(description);
  const [isClickName, setIsClickName] = useState(false);
  const isDealer = me.role === Dealer;
  const isRoundRun = useSelector(selectIsRoundRun);
  const currentIssueId = useSelector(selectCurrentIssueId);

  const dispatch = useDispatch();

  useEffect(() => {
    setChipsTitle(title);
  }, [title]);

  useEffect(() => {
    setChipsDescription(description);
  }, [description]);

  const handleEditTitle = (value: string) => {
    value && setChipsTitle(value.slice(0, 90));
    value && dispatch(changeIssueTitle({ id, title: value.slice(0, 90) }));
  };

  const handleEditDescription = (value: string) => {
    setChipsDescription(value.slice(0, 1000));
    dispatch(changeIssueDescription({ id, description: value.slice(0, 1000) }));
  };

  const handleViewTitle = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
    e.stopPropagation();
    if (isRoundRun) return;
    () => setIsClickName(!isClickName);
  };

  const handleViewDescription = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
    e.stopPropagation();
    if (isRoundRun) return;
    () => setIsClickName(!isClickName);
  };

  useEffect(() => {
    setChipsTitle(title);
  }, [title]);

  useEffect(() => {
    setChipsDescription(description);
  }, [description]);

  const handleDelete = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
    e.stopPropagation();
    if (isRoundRun) return;
    dispatch(removeIssue(id));
  };

  const contentTitle = (
    <div>
      <Text
        className={`issue-chips__description ${isClickName && '_active'}`}
        editable={
          isDealer
            ? {
                onChange: (value) => handleEditTitle(value),
                autoSize: true,
              }
            : false
        }
      >
        {chipsTitle}
      </Text>
    </div>
  );

  const contentDescription = (
    <div>
      <Text
        className={`issue-chips__description ${isClickName && '_active'}`}
        editable={
          isDealer
            ? {
                onChange: (value) => handleEditDescription(value),
                autoSize: true,
              }
            : false
        }
      >
        {chipsDescription?.length ? chipsDescription : 'No description yet...'}
      </Text>
    </div>
  );

  return (
    <div
      className={`issue-chips issue-field ${isClickName && '_active'} ${
        id === currentIssueId ? 'current' : ''
      }`}
    >
      <div className="issue-chips__content">
        <Paragraph data-space="home" className="issue-chips__name">
          {chipsTitle}
        </Paragraph>

        <Popover content={contentTitle} placement="bottom" trigger="click">
          <Tooltip
            title="Title"
            overlayInnerStyle={{ color: 'black' }}
            color={colorWhite}
            key={colorWhite}
          >
            <FormOutlined className="issue-chips__edit" onClick={handleViewTitle} />
          </Tooltip>
        </Popover>

        <Popover content={contentDescription} placement="bottom" trigger="click">
          <Tooltip
            title="Description"
            overlayInnerStyle={{ color: 'black' }}
            color={colorWhite}
            key={colorWhite}
          >
            <InfoCircleOutlined className="issue-chips__more" onClick={handleViewDescription} />
          </Tooltip>
        </Popover>
        {isDealer && (
          <Tooltip
            title="Delete"
            overlayInnerStyle={{ color: 'black' }}
            color={colorWhite}
            key={colorWhite}
          >
            <DeleteOutlined className="issue-chips__delete" onClick={handleDelete} />
          </Tooltip>
        )}
      </div>
      <Text className="issue-chips__priority">{priority}</Text>
    </div>
  );
};
