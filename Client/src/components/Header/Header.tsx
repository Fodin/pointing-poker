import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Badge } from 'antd';
import { WechatOutlined } from '@ant-design/icons';

import { Chat } from '../Chat/Chat';
import { Logo } from '../Logo/Logo';

import { selectChat, selectGame } from '../../redux/selectors';

import './style.scss';

const chatOpen = '#ffff00';
const chatClosed = '#ffffff';
const lobbyPage = 1;
const gamePage = 2;

export const Header = (): JSX.Element => {
  const [isChatOpen, setIsChatOpen] = useState(false);

  const game = useSelector(selectGame);

  const chat = useSelector(selectChat);

  return (
    <header className="header">
      <div className="header_wrapper">
        <div className="header_content">
          <h1>Poker Planning</h1>
          <Logo />
          {game.currentPage === lobbyPage || game.currentPage === gamePage ? (
            <Badge count={chat.length} size="small">
              <WechatOutlined
                className="chat-icon"
                style={{ color: `${isChatOpen ? chatOpen : chatClosed}` }}
                onClick={() => setIsChatOpen(!isChatOpen)}
              />
            </Badge>
          ) : null}
        </div>
      </div>
      <Chat isChatOpen={isChatOpen} setIsChatOpen={setIsChatOpen} />
    </header>
  );
};
