import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setIsRoundRun, setResultCardValue } from '../../redux/actions';
import {
  selectCurrentIssueId,
  selectIsRoundRun,
  selectSettings,
  selectStatistic,
  selectUsers,
} from '../../redux/selectors';
import { UserRole } from '../../shared/enums';
import { useContainerWidth } from '../../shared/hook';
import { getOffsetToLeftForOneItem } from '../../shared/utils';
import { GameCardPlay } from '../GameCardPlay/GameCardPlay';
import './style.scss';

const WIDTH_CARD = 120;

export const GameCardsField = (): JSX.Element => {
  const [selectedCard, setSelectedCard] = useState<string | number>('');
  const dispatch = useDispatch();
  const isRoundRun = useSelector(selectIsRoundRun);
  const { scoreType, cardSet } = useSelector(selectSettings);
  const users = useSelector(selectUsers);
  const statistic = useSelector(selectStatistic);
  const currentIssueId = useSelector(selectCurrentIssueId);
  const { Observer, Dealer } = UserRole;

  const componentRef = useRef(null);
  const containerWidth = useContainerWidth(componentRef);

  const handleClick = (value: string | number) => {
    if (isRoundRun) {
      setSelectedCard(value);
      dispatch(setResultCardValue(value));
      clickUserOnCard;
    }
  };

  const clickUserOnCard = useEffect(() => {
    const numberUserVote = users.filter((user) => user.role !== Observer).length;
    const numberVotesCurrentIssue = statistic.find((result) => result.issueId === currentIssueId)
      ?.votes.length;
    if (numberUserVote === numberVotesCurrentIssue) {
      dispatch(setIsRoundRun(false));
    }
  }, [statistic]);

  useEffect(() => {
    if (!isRoundRun) {
      setSelectedCard('');
    }
  }, [isRoundRun, dispatch]);

  return (
    <div className="game-cards-field" ref={componentRef}>
      {cardSet.map((cardValue, idx) => (
        <GameCardPlay
          card={{ id: idx, value: cardValue, logo: scoreType }}
          absoluteLeft={getOffsetToLeftForOneItem(containerWidth, cardSet.length, WIDTH_CARD) * idx}
          key={idx}
          isCurrent={cardValue === selectedCard}
          onclick={handleClick}
        />
      ))}
    </div>
  );
};
