import React from 'react';
import { Typography } from 'antd';
import { Statistic } from '../Statistic/Statistic';
import './style.scss';
import { IIssueResult } from '../../shared/types';
const { Title } = Typography;

export const RecultItem = ({ issue }: { issue: IIssueResult }): JSX.Element => {
  return (
    <div className="result-item section-field ">
      <Title level={4}>{`Issue: ${issue.title}`}</Title>
      <div className="result-item__description">
        <span className="result-item__description-title">Description: </span>
        {issue.description}
      </div>
      <Title level={5}>{`Average result: ${issue.averageResult}`}</Title>
      <Statistic issueId={issue.id} />
    </div>
  );
};
