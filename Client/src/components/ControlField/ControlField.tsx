import React, { useCallback } from 'react';
import { Button, Col, Row, Typography } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import {
  clearResultCurrentIssue,
  setCurrentIssueId,
  setCurrentPage,
  setIsRoundRun,
} from '../../redux/actions';
import {
  selectCurrentIssueId,
  selectIsRoundRun,
  selectIssues,
  selectSettings,
  selectStatistic,
} from '../../redux/selectors';
import { Timer } from '../Timer/Timer';

import './style.scss';

const { Title } = Typography;

interface IControlField {
  isMaster: boolean;
}

export const ControlField = ({ isMaster }: IControlField): JSX.Element => {
  const isRoundRun = useSelector(selectIsRoundRun);
  const settings = useSelector(selectSettings);
  const result = useSelector(selectStatistic);
  const currentIssueId = useSelector(selectCurrentIssueId);
  const issues = useSelector(selectIssues);

  const hasIssueResult = useCallback(
    (idFind): boolean => {
      let hasIssue = false;
      result.forEach((resultIssue) => {
        if (resultIssue.issueId === idFind) {
          if (resultIssue.votes.length > 0) {
            hasIssue = true;
          }
        }
      });
      return hasIssue;
    },
    [result],
  );

  const dispatch = useDispatch();

  const handleRunRound = () => {
    dispatch(setIsRoundRun(true));
    if (hasIssueResult(currentIssueId)) {
      dispatch(clearResultCurrentIssue());
    }
  };

  const handleStopRound = () => {
    dispatch(setIsRoundRun(false));
  };

  const onNextIssue = () => {
    const indexCurrenIssue = issues.findIndex((issue) => issue.id === currentIssueId);
    let nextCurrentIssueId;
    issues.find((issue, index, arr) => {
      const id = arr[(index + indexCurrenIssue + 1) % issues.length].id;
      nextCurrentIssueId = id;
      if (!hasIssueResult(id)) return true;
      return false;
    });
    nextCurrentIssueId && dispatch(setCurrentIssueId(nextCurrentIssueId));
  };

  const onStopGame = () => {
    dispatch(setCurrentPage(3));
  };

  const handleTimerFinish = () => {
    dispatch(setIsRoundRun(false));
  };

  return (
    <div className="game-page__control-field section-field">
      <Title level={4}>Control panel:</Title>
      {settings.isTimerNeeded && (
        <Timer timerValue={settings.roundTime} isStart={isRoundRun} onFinish={handleTimerFinish} />
      )}
      {isMaster && (
        <Row justify={'center'} gutter={[16, 16]} wrap={true} style={{ marginTop: 20 }}>
          <Col span={24}>
            <Button
              className="game-page__btn"
              type="primary"
              htmlType="button"
              shape="round"
              size="large"
              onClick={isRoundRun ? handleStopRound : handleRunRound}
            >
              {isRoundRun
                ? 'Stop round'
                : hasIssueResult(currentIssueId)
                ? 'Restart round'
                : 'Run round'}
            </Button>
          </Col>
          <Col span={24}>
            <Button
              disabled={isRoundRun}
              className="game-page__btn"
              type="primary"
              htmlType="button"
              shape="round"
              size="large"
              onClick={onNextIssue}
            >
              Next issue
            </Button>
          </Col>
          <Col span={24}>
            <Button
              disabled={isRoundRun}
              className="game-page__btn"
              type="default"
              htmlType="button"
              shape="round"
              size="large"
              danger
              onClick={onStopGame}
            >
              {'End Game'}
            </Button>
          </Col>
        </Row>
      )}
    </div>
  );
};
