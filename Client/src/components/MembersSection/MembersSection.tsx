import React from 'react';
import { Col, Typography } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { PlayerCard } from '../PlayerCard/PlayerCard';

import { removeUser } from '../../redux/actions';
import { selectUsers } from '../../redux/selectors';

import './style.scss';

const { Title } = Typography;

export const MembersSection = (): JSX.Element => {
  const users = useSelector(selectUsers);

  const dispatch = useDispatch();

  return (
    <Col flex="1 1 200px" className="members-section section-field">
      <Title level={4}>Members:</Title>
      <div className="members-section__members">
        {users.map((user, id) => (
          <PlayerCard
            onDeletePlayer={() => dispatch(removeUser(user.id))}
            user={user}
            hasBtn={true}
            key={id}
          />
        ))}
      </div>
    </Col>
  );
};
