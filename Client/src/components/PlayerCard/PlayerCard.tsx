import './style.scss';
import React from 'react';
import { Avatar } from 'antd';
import { IUser } from '../../shared/types';
import { CloseCircleOutlined } from '@ant-design/icons';
import { getColor, getFirstCharsOfWords } from '../../shared/utils';
import { UserRole } from '../../shared/enums';

import { me } from '../../shared/localData';
const { Dealer, Observer } = UserRole;

interface IPlayerCard {
  user: IUser;
  onDeletePlayer?: () => void;
  hasBtn?: boolean;
  scale?: number;
}

export const PlayerCard = ({ user, scale = 1 }: IPlayerCard): JSX.Element => {
  const { id, firstName, lastName, role, position } = user;

  const handleDeletePlayer = () => {
    console.log('UserId: %s, firstName: %s', id, firstName);
    return;
  };

  return (
    <div
      className={`player-card ${id === me.id && 'player-card_its-you'}`}
      style={{ height: `${60 * scale}px`, fontSize: `${10 * scale}px` }}
    >
      <Avatar
        className="player-card__avatar"
        size={50 * scale}
        src={user?.avatar}
        style={{
          backgroundColor: getColor(id),
          fontSize: 10 * scale,
        }}
      >
        {getFirstCharsOfWords(firstName, lastName)}
      </Avatar>
      <div className="player-card__content">
        <p className="player-card__it-you">{role === Dealer && `Dealer`}</p>

        <p className="player-card__name">
          {role === Observer && <span>&#128065;</span>}
          {` ${firstName} ${lastName ? lastName : ''}`}
        </p>
        {position && <p className="player-card__position">{`${position}`}</p>}
      </div>

      {role !== Dealer && me.role === Dealer && (
        <CloseCircleOutlined className="player-card__btn" onClick={handleDeletePlayer} />
      )}
    </div>
  );
};
