import React from 'react';
import { useState, useEffect } from 'react';
import { getMinutes, getSeconds } from '../../shared/utils';
import './style.scss';

interface ITimer {
  timerValue: number;
  isStart: boolean;
  onFinish: () => void;
}

export const Timer = ({ timerValue, isStart, onFinish }: ITimer): JSX.Element => {
  const [timerVal, setTimerVal] = useState<number>(0);

  useEffect(() => {
    if (!isStart) return;
    if (timerVal < 0) onFinish();

    const interval = setInterval(() => {
      setTimerVal((prevTimerValue) => prevTimerValue - 1);
    }, 1000);
    return () => clearInterval(interval);
  }, [timerVal, isStart, onFinish]);

  useEffect(() => {
    setTimerVal(timerValue);
  }, [timerValue, isStart]);

  return (
    <div className="clock">
      <div className="clock__title">
        <span className="clock__title-item">minutes</span>
        <span className="clock__title-item">seconds</span>
      </div>
      <div className="clock__digits">{`${getMinutes(timerVal)} : ${getSeconds(timerVal)}`}</div>
    </div>
  );
};
