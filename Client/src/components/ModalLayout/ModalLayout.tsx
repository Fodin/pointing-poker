import React from 'react';
import './style.scss';
import { Modal } from 'antd';

interface IModalLayout {
  isModalVisible: boolean;
  onCloseForm: () => void;
  children: JSX.Element;
}

export const ModalLayout = ({
  onCloseForm,
  isModalVisible,
  children,
}: IModalLayout): JSX.Element => {
  return (
    <Modal
      visible={isModalVisible}
      onOk={() => console.log('form ok')}
      onCancel={() => onCloseForm()}
      footer={null}
    >
      {children}
    </Modal>
  );
};
