import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentIssueId } from '../../redux/actions';
import { selectGame, selectIsRoundRun, selectStatistic } from '../../redux/selectors';
import './style.scss';
import Tick from '../../assets/tick.svg';

interface IIssueChipsPlay {
  issueId: string;
  children: JSX.Element;
}

export const IssueChipsPlay = ({ issueId, children }: IIssueChipsPlay): JSX.Element => {
  const isRoundRun = useSelector(selectIsRoundRun);

  const game = useSelector(selectGame);
  const result = useSelector(selectStatistic);
  const dispatch = useDispatch();

  const handleClick = () => {
    if (isRoundRun || game.currentPage !== 2) return;
    dispatch(setCurrentIssueId(issueId));
  };

  const hasIssueResult = useCallback((): boolean => {
    let hasIssue = false;
    result.forEach((resultIssue) => {
      if (resultIssue.issueId === issueId) {
        if (resultIssue.votes.length > 0) {
          hasIssue = true;
        }
      }
    });
    return hasIssue;
  }, [isRoundRun]);

  return (
    <div className="issue-chips-play" onClick={handleClick}>
      {hasIssueResult() && (
        <Tick style={{ position: 'absolute', zIndex: 200, left: 10, top: -5 }} />
      )}
      {children}
    </div>
  );
};
