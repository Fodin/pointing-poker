import React from 'react';
import { Result } from 'antd';
import { useSelector } from 'react-redux';

import { selectError } from '../../redux/selectors';

import './style';

export function FatalError(): JSX.Element {
  const error = useSelector(selectError);
  return (
    <Result
      className="fatal-error-page"
      status="error"
      title={error.title}
      subTitle={error.description}
    />
  );
}
