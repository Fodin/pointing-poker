import React from 'react';

import './style.scss';

export const Overlay: React.FC<{
  children: JSX.Element;
}> = ({ children }) => {
  return <div className="overlay">{children}</div>;
};
