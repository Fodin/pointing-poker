import React from 'react';

import { Button, Typography } from 'antd';

import './style.scss';

const { Title, Text } = Typography;

export const KickModal: React.FC<{
  isMaster?: boolean;
  kickUser: string;
  user?: string;
}> = ({ isMaster = true, kickUser, user }): JSX.Element => {
  return (
    <div className="kick-modal">
      <Title level={2}>{isMaster ? 'Kick player?' : 'Kick'}</Title>
      <Text className="kick-modal__text">
        {isMaster
          ? `Are you really want to remove playe ${kickUser} from game session?`
          : `${user} want to kick member ${kickUser}. Do you agree with it? `}
      </Text>
      <div className="kick-modal__btns">
        <Button
          className="kick-modal__btn"
          type="primary"
          htmlType="submit"
          shape="round"
          size="middle"
        >
          Yes
        </Button>
        <Button
          className="kick-modal__btn"
          type="default"
          htmlType="button"
          shape="round"
          size="middle"
          danger
        >
          No
        </Button>
      </div>
    </div>
  );
};
