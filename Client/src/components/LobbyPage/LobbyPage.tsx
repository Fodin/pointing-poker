import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Row } from 'antd';

import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { AdminSection } from '../AdminSection/AdminSection';
import { MembersSection } from '../MembersSection/MembersSection';
import { GameSettingsSection } from '../GameSettingsSection/GameSettingsSection';
import { IssuesSection } from '../IssuesSection/IssuesSection';

import { addCardValue, setSettings } from '../../redux/actions';

import { me } from '../../shared/localData';
import { UserRole } from '../../shared/enums';

import './style';

const { Dealer } = UserRole;

export const LobbyPage = (): JSX.Element => {
  const dispatch = useDispatch();

  useEffect(() => {
    const settingsFromLocalStorage = localStorage.getItem('settings');
    const cardArrFromLocalStorage = localStorage.getItem('cardArray');
    if (me.role === Dealer && settingsFromLocalStorage) {
      const settingsFromLocalStorageObj = JSON.parse(settingsFromLocalStorage);
      dispatch(setSettings(settingsFromLocalStorageObj));
    }
    if (me.role === Dealer && cardArrFromLocalStorage) {
      const cardArrFromLocalStorageObj = JSON.parse(cardArrFromLocalStorage);
      dispatch(addCardValue(cardArrFromLocalStorageObj));
    }
  }, []);

  return (
    <div className="lobby-page">
      <Header />
      <main className="lobby-page__main">
        <div className="lobby-page__container">
          <AdminSection />
          <Row gutter={[0, 32]} className="lobby-page__content">
            <MembersSection />
            <IssuesSection />
          </Row>
          <GameSettingsSection />
        </div>
      </main>
      <Footer />
    </div>
  );
};
