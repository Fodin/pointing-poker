import { Router } from "express";
import path from "path";
import fetch from "node-fetch";

const router = Router();


router.route("/:id").get((req, res, next) => {
  if (isValidKey(req.params.id)) {
    res.sendFile(path.join(path.resolve(), "/client/index.html"));
  } else {
    next();
  }
});

router.route("/avatar").get((req, res) => {
  if (req) {
  fetch('https://i.pravatar.cc/64')
  .then(response => { 
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', '*'); 
    res.setHeader('Content-Type', 'image/jpeg',);

    response.arrayBuffer().then((buffer) => {
      const base64Flag = 'data:image/jpeg;base64,';
      const imageStr = arrayBufferToBase64(buffer);
      res.send(base64Flag + imageStr);
    });
  }) 
}
});

export function arrayBufferToBase64(key: ArrayBuffer): string {
  return Buffer.from(key)
    .toString("base64");
}

export default router;

function isValidKey(key: string): boolean {
  return key.length === 21 && /^[a-z\d-_]*$/i.test(key);
}

