import express, {
  ErrorRequestHandler,
  NextFunction,
  Request,
  Response,
} from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import dotenv from "dotenv";

import gameRouter from "./gameRouter.js";
import {
  addGame,
  addUser,
  deleteGame,
  deleteUser,
  getDataBySocketId,
  getDataByUserId,
  getDealer,
  isGameExist,
  user,
  UserRole,
} from "./users.js";
import { logger } from "./logger.js";

dotenv.config();
const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  serveClient: true,
  cors: {
    origin: process.env["CORS_ORIGIN"],
  },
});

io.on("connection", (socket) => {
  type handShakeParam = string | string[] | undefined;
  const gameId: handShakeParam = socket.handshake.query["gameId"];
  const userId: handShakeParam = socket.handshake.query["userId"];
  const userRole: handShakeParam = socket.handshake.query["userRole"];
  if (
    typeof gameId !== "string" ||
    typeof userId !== "string" ||
    typeof userRole !== "string"
  ) {
    socket.disconnect();
    return;
  }

  if (getDataByUserId(userId)?.gameId === gameId) {
    // User already connected to game with this gameId
    logger.error(
      `User (id: ${userId}) is already connected to game id: ${gameId}!`
    );
    socket.send({
      type: "STATE_UPDATE",
      value: [
        [
          "set error",
          {
            title: "Error!",
            description: "Sorry, this user already connected to the game!",
          },
        ],
        ["set game.currentPage", 4],
      ],
    });
    socket.disconnect();
    return;
  }

  const user: user = {
    socketId: socket.id,
    userId,
    userRole: userRole as UserRole,
  };

  if (userRole === UserRole.Dealer) {
    addGame(gameId);
    addUser(gameId, user);
  } else {
    if (!isGameExist(gameId)) {
      logger.error(`Game id: ${gameId} is not found!`);
      socket.emit("dealerDisconnected", `Game id: ${gameId} is not found!`);
      return;
    }
    addUser(gameId, user);
  }

  socket.join(gameId);

  logger.info(`User (id: ${userId}) connected to game: ${gameId}`);

  socket.on("message", (message) => {
    // console.log("%o", message);
    const userData = getDataBySocketId(socket.id);
    // console.log('Sender data: %o', userData);
    let dealer;
    if (userData) {
      dealer = getDealer(userData.gameId);
      if (dealer) {
        switch (userData.user.userRole) {
          case UserRole.Dealer:
            socket.broadcast.to(userData.gameId).emit("message", message);
            break;
          case UserRole.Player:
          case UserRole.Observer:
            io.to(dealer.socketId).emit("message", {
              ...message,
              senderId: userData.user.userId,
            });
            break;
        }
      }
    }
  });

  // Used only for transfer initial state on connect
  socket.on("dealerToUser", (message) => {
    if (userRole === UserRole.Dealer) {
      const userData = getDataByUserId(message.senderId);
      // console.log('userdata ');
      // console.log(userData);
      if (userData) {
        const action = { type: message.type, value: message.value };
        // console.log('action');
        // console.log(action);
        socket.to(userData.user.socketId).emit("message", action);
      }
    }
  });

  socket.on("disconnect", (reason) => {
    if (userRole === UserRole.Dealer) {
      deleteGame(gameId);
      io.to(gameId).emit("dealerDisconnected");
      logger.error(
        `Dealer id: ${userId} disconnected from game: ${gameId} because of "${reason}"`
      );
    } else {
      deleteUser(gameId, userId);
      const dealer = getDealer(gameId);
      dealer && io.to(dealer.socketId).emit("userDisconnected", user);
      logger.info(
        `User (id: ${userId}) disconnected from game: ${gameId} because of "${reason}"`
      );
      // console.log('userId: %s user: %o', userId, user  );
    }
  });
});

app.use("/", gameRouter);

app.use(express.static("client", { fallthrough: false }));

app.use(function (
  _err: ErrorRequestHandler,
  _req: Request,
  res: Response,
  _next: NextFunction
) {
  res.send(
    "Sorry, nothing has found. You can use only root link for create game or a link your dealer sent you."
  );
});

httpServer.listen(process.env["PORT"] || 3000);
