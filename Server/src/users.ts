export enum UserRole {
  Dealer = "0",
  Player = "1",
  Observer = "2",
}

export type user = {
  socketId: string;
  userId: string;
  userRole: UserRole;
};

export const games: Map<string, user[]> = new Map();

export function addGame(gameId: string): void {
  if (!isGameExist(gameId)) {
    games.set(gameId, []);
  }
}

export function deleteGame(gameId: string): void {
  games.delete(gameId);
}

export function isGameExist(gameId: string): boolean {
  return games.has(gameId);
}

export function getDataBySocketId(socketId: string):
  | {
      gameId: string;
      user: user;
    }
  | undefined {
  let user: user | undefined;

  for (let game of games) {
    user = game[1].find((el) => socketId === el.socketId);
    if (user) {
      return { gameId: game[0], user };
    }
  }
  return undefined;
}

export function getDataByUserId(userId: string):
  | {
      gameId: string;
      user: user;
    }
  | undefined {
  let user: user | undefined;

  for (let game of games) {
    user = game[1].find((el) => userId === el.userId);
    if (user) {
      return { gameId: game[0], user };
    }
  }
  return undefined;
}

export function addUser(gameId: string, user: user): void {
  if (isGameExist(gameId)) {
    games.get(gameId)!.push(user);
  }
}

export function deleteUser(gameId: string, userId: string): void {
  if (isGameExist(gameId)) {
    games.set(
      gameId,
      games.get(gameId)!.filter((user) => user.userId !== userId)
    );
  }
}

export function getDealer(gameId: string): user | null {
  if (!isGameExist(gameId)) {
    return null;
  }
  return (
    games.get(gameId)!.filter((user) => user.userRole === UserRole.Dealer)[0] ||
    null
  );
}
