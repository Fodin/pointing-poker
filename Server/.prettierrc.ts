export default {
  trailingComma: "all",
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  bracketSpacing: true,
  printWidth: 100,
  endOfLine: "lf",
  useTabs: false,
  jsxBracketSameLine: false,
  arrowParens: 'always',
  jsxSingleQuote: true,
};
